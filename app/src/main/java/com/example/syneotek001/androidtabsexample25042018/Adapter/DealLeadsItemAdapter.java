package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.CreateDealFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.DealLeadsModel;

import java.util.ArrayList;
import java.util.List;

public class DealLeadsItemAdapter extends RecyclerView.Adapter<DealLeadsItemAdapter.MyViewHolder> {
    private Context mContext;
    private List<DealLeadsModel> mData;

    class MyViewHolder extends ViewHolder {
        public TextView tvCustomerId,tvDateTime,tvMakeDeal,tvName;

        MyViewHolder(View view) {
            super(view);
            tvCustomerId=view.findViewById(R.id.tvCustomerId);
            tvDateTime=view.findViewById(R.id.tvDateTime);
            tvMakeDeal=view.findViewById(R.id.tvMakeDeal);
            tvName=view.findViewById(R.id.tvName);
        }
    }

    public DealLeadsItemAdapter(Context context, ArrayList<DealLeadsModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public DealLeadsItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deal_leads, parent, false);

        return new DealLeadsItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DealLeadsModel mDealLeadsModel = this.mData.get(position);
        holder.tvName.setText(mDealLeadsModel.getCustomerName());
        holder.tvCustomerId.setText(mDealLeadsModel.getCustomerId());
        holder.tvDateTime.setText(mDealLeadsModel.getDateTime());
        holder.tvMakeDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)mContext).replaceFragment(new CreateDealFragment());
            }
        });
    }

    public int getItemCount() {
        return mData.size();
    }
}
