package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.ClientItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class ClientItemAdapter extends RecyclerView.Adapter<ClientItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<ClientItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (ClientItemAdapter.this.onItemClickListener != null) {
                    ClientItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            /*case R.id.ivLeadLabel:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;*/
            case R.id.ivEmail:
                if (ClientItemAdapter.this.onItemClickListener != null) {
                    ClientItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            /*case R.id.ivEdit:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;*/
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEmail, ivInfo, ivEdit;
        LinearLayout llActionItems;
        LinearLayout ll_Phone;
        TextView tvClientType, tvName, tvEmail;
        View viewDummySpace;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEmail = view.findViewById(R.id.ivEmail);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivInfo = view.findViewById(R.id.ivInfo);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvClientType = view.findViewById(R.id.tvClientType);
            tvEmail = view.findViewById(R.id.tvEmail);
            tvName = view.findViewById(R.id.tvName);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);



            ll_Phone = view.findViewById(R.id.ll_Phone);


        }

        @Override
        public void onClick(View v) {

        }
    }

    public ClientItemAdapter(Context context, ArrayList<ClientItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_client, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        ClientItemModel mLeadItem = (ClientItemModel) mData.get(position);
        holder.tvName.setText(mLeadItem.getClientName());
        holder.tvEmail.setText(mLeadItem.getClientEmail());
        if (mLeadItem.getClinetType().equals("1")) {
            holder.tvClientType.setText(mContext.getString(R.string.str_walk_in));
            holder.tvClientType.setBackgroundResource(R.drawable.bg_task_status_pending);
        } else if (mLeadItem.getClinetType().equals("2")) {
            holder.tvClientType.setText(mContext.getString(R.string.str_phone_call));
            holder.tvClientType.setBackgroundResource(R.drawable.bg_task_status_not_reply);
        } else if (mLeadItem.getClinetType().equals("3")) {
            holder.tvClientType.setText(mContext.getString(R.string.str_appointment));
            holder.tvClientType.setBackgroundResource(R.drawable.bg_task_status_follow_up);
        } else if (mLeadItem.getClinetType().equals("4")) {
            holder.tvClientType.setText(mContext.getString(R.string.str_be_back));
            holder.tvClientType.setBackgroundResource(R.drawable.bg_task_status_completed);
        }


        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add((holder.ll_Phone));
            viewList.add((holder.tvClientType));
            viewList.add(holder.viewDummySpace);
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.ll_Phone);
        viewList.add(holder.tvClientType);
        viewList.add(holder.viewDummySpace);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    ClientItemModel mLeadItem = (ClientItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });


        holder.ivInfo.setOnClickListener(this);
        holder.ivEmail.setOnClickListener(this);
        holder.ivEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ClientItemAdapter.this.onItemClickListener != null) {
                    ClientItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(position);
                    return;
                }
            }
        });


    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
