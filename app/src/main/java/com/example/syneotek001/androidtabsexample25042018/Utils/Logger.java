package com.example.syneotek001.androidtabsexample25042018.Utils;


import com.example.syneotek001.androidtabsexample25042018.BuildConfig;

public class Logger {
    private static String TAG = BuildConfig.APPLICATION_ID;

    public static void m5e(String Msg) {
        LogIt(6, TAG, Msg);
    }

    public static void m6e(String Tag, String Msg) {
        LogIt(6, Tag, Msg);
    }

    public static void m7i(String Msg) {
        LogIt(4, TAG, Msg);
    }

    public static void m8i(String Tag, String Msg) {
        LogIt(4, Tag, Msg);
    }

    public static void m3d(String Msg) {
        LogIt(3, TAG, Msg);
    }

    public static void m4d(String Tag, String Msg) {
        LogIt(3, Tag, Msg);
    }

    public static void m9v(String Msg) {
        LogIt(2, TAG, Msg);
    }

    public static void m10v(String Tag, String Msg) {
        LogIt(2, Tag, Msg);
    }

    public static void m11w(String Msg) {
        LogIt(5, TAG, Msg);
    }

    public static void m12w(String Tag, String Msg) {
        LogIt(5, Tag, Msg);
    }

    private static void LogIt(int LEVEL, String Tag, String Message) {
    }
}
