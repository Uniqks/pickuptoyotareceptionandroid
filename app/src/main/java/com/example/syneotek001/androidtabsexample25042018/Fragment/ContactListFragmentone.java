package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.ContactIndexAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.ContactListAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.HeaderItemDecoration;
import com.example.syneotek001.androidtabsexample25042018.Utils.RecyclerViewFastScroller;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ContactIndex;
import com.example.syneotek001.androidtabsexample25042018.model.ContactListModel;

import java.util.ArrayList;
import java.util.Random;
import java.util.TreeMap;


public class ContactListFragmentone extends Fragment implements OnBackPressed, ContactIndexAdapter.ContactIndexClick {
    ContactListAdapter mAdapter;
    ArrayList<ContactListModel> mContactList = new ArrayList<>();
    ArrayList<ContactListModel> mContactListTemp = new ArrayList<>();
    TreeMap<ContactListModel, String> treeMap = new TreeMap<>();
    View view;

    FloatingActionButton fabAddContact;
    public RecyclerView recyclerView;
    public RecyclerViewFastScroller rvFastScroller;
    public TextView tvNoContactFound, tvTitle;

    RecyclerView lvIndex;
    ContactIndexAdapter adapter;
    ArrayList<ContactIndex> arrayList = new ArrayList<>();
    String[] arrContactIndex = {"ALL","A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    String[] arrContactIndexValue = {"13","4", "5", "2", "0", "0", "0", "0", "0", "0", "0", "0", "0", "1", "0", "0", "0", "0", "0", "1", "0", "0", "0", "0", "0", "0", "0"};

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_contact_listone, container, false);
        recyclerView = rootview.findViewById(R.id.recyclerView);
        tvNoContactFound = rootview.findViewById(R.id.tvNoContactFound);
        rvFastScroller = rootview.findViewById(R.id.rvFastScroller);
        fabAddContact = rootview.findViewById(R.id.fabAddContact);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        lvIndex = rootview.findViewById(R.id.lvIndex);



        ImageView ivBack = rootview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setUpRecyclerView();
        setHasOptionsMenu(false);
        setClickEvents();

        for (int i=0;i<arrContactIndex.length;i++) {
            arrayList.add(new ContactIndex(arrContactIndex[i],arrContactIndexValue[i],false));
        }

        arrayList.get(0).setIs_selected(true);

        adapter = new ContactIndexAdapter(getActivity(),arrayList);
        adapter.setContactIndexClick(ContactListFragmentone.this);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        lvIndex.setLayoutManager(layoutManager);
        lvIndex.setAdapter(adapter);

        return rootview;
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onContactIndexClick(int position) {
        arrayList.clear();
        for (int i=0;i<arrContactIndex.length;i++) {
            arrayList.add(new ContactIndex(arrContactIndex[i],arrContactIndexValue[i],false));
        }
        arrayList.get(position).setIs_selected(true);
        adapter.notifyDataSetChanged();

        if (position!=0){
            mContactListTemp.clear();
            for (int i=0;i<mContactList.size();i++) {
                if (mContactList.get(i).getFirstName().startsWith(arrayList.get(position).getIndex_name()))
                mContactListTemp.add(mContactList.get(i));
            }
            //mAdapter.notifyDataSetChanged();
        } else {
            for (int i = 0; i < mContactList.size(); i++) {
                mContactListTemp.add(mContactList.get(i));
            }
        }

        Log.e("contact",mContactListTemp.size()+" pl "+mContactList.size()+" ");
           // mAdapter.notifyDataSetChanged();
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
            mAdapter = new ContactListAdapter(getActivity(), this.mContactListTemp);
            recyclerView.addItemDecoration(new HeaderItemDecoration(recyclerView, mAdapter));
            setFastScrollerLayoutManager();
            recyclerView.setAdapter(this.mAdapter);
            recyclerView.addOnScrollListener(new OnScrollViewClick());
            refreshData();



    }

    class onClick implements OnClickListener {
        onClick() {
        }

        public void onClick(View view) {

//            Toast.makeText(getActivity(), "AddEditContactMainFragment", Toast.LENGTH_SHORT).show();
            ((HomeActivity) getActivity()).replaceFragment(new AddEditContactMainFragment());
//            ((MainActivity) getActivity()).pushFragment(((MainActivity) getActivity()).mCurrentTab, new AddEditContactMainFragment(), true, true);
        }
    }

    class OnScrollViewClick extends OnScrollListener {
        OnScrollViewClick() {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0 && fabAddContact.isShown()) {
                fabAddContact.hide();
            } else if (dy < 0 && !fabAddContact.isShown()) {
                fabAddContact.show();
            }
        }
    }

    static class onTouchListener implements OnTouchListener {
        FloatingActionButton fabAddContact;

        onTouchListener() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 0) {
                fabAddContact.hide();
            } else if (event.getAction() == 1) {
                fabAddContact.show();
            }
            return false;
        }
    }

    private void setClickEvents() {
        fabAddContact.setOnClickListener(new onClick());
    }

    private void setUpRecyclerView() {

        ContactListModel mContactModel = new ContactListModel("1", "Abc", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Chotu", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Bbc", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("3", "Mayank", "Bhandari", "mkbhandari@gmail.com", "7698523697");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Bb", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("2", "Suresh", "Shah", "suresh.shah@gmail.com", "9956324896");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Aaa", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Aa", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Bbcd", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Chotu", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Aaaa", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Bk", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        mContactModel = new ContactListModel("1", "Bhai", "Patel", "abc@gmail.com", "9876543210");
        this.treeMap.put(mContactModel, mContactModel.getFirstName());
        String previousLetter = "";
        mContactList = new ArrayList<>();
        for (ContactListModel objContact : this.treeMap.keySet()) {
            Log.e("contact",treeMap.size()+" pl "+previousLetter+" "+objContact.getFirstName());
            objContact.setLetter(Character.toString(objContact.getFirstName().charAt(0)));
            if (previousLetter.length() == 0 || !previousLetter.equals(objContact.getLetter())) {
                objContact.setHeader(true);
            } else {
                objContact.setHeader(false);
            }
            previousLetter = objContact.getLetter();

            mContactList.add(objContact);
        }

        for (int i=0;i<mContactList.size();i++) {
            mContactListTemp.add(mContactList.get(i));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        mAdapter = new ContactListAdapter(getActivity(), this.mContactListTemp);
        recyclerView.addItemDecoration(new HeaderItemDecoration(recyclerView, mAdapter));
        setFastScrollerLayoutManager();
        recyclerView.setAdapter(this.mAdapter);
        recyclerView.addOnScrollListener(new OnScrollViewClick());
        refreshData();
    }

    private void setFastScrollerLayoutManager() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false) {
            public void onLayoutChildren(Recycler recycler, State state) {
                int i = 8;
                super.onLayoutChildren(recycler, state);
                int firstVisibleItemPosition = findFirstVisibleItemPosition();
                if (firstVisibleItemPosition == 0) {
                    int itemsShown = (findLastVisibleItemPosition() - firstVisibleItemPosition) + 1;
                    RecyclerViewFastScroller recyclerViewFastScroller = rvFastScroller;
                    if (mAdapter.getItemCount() > itemsShown) {
                        i = 0;
                    }
                    recyclerViewFastScroller.setVisibility(i);
                } else if (firstVisibleItemPosition == -1) {
                    rvFastScroller.setVisibility(View.GONE);
                }
            }
        });
        rvFastScroller.setRecyclerView(recyclerView);
//        rvFastScroller.setViewsToUse(R.layout.recycler_view_fast_scroller__fast_scroller, R.id.fastscroller_bubble, R.id.fastscroller_handle);
        rvFastScroller.setOnTouchListener(new onTouchListener());
    }

    public void onResume() {
        super.onResume();
        rvFastScroller.setRecyclerView(recyclerView);
    }

    private void refreshData() {
        if (this.mContactListTemp.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoContactFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoContactFound.setVisibility(View.VISIBLE);
    }

    public int generateRandom() {
        int min = 20;
        int max = 80;
        Random r = new Random();
        int random = r.nextInt((max - min) + 1) + min;
        return random;
    }

}
