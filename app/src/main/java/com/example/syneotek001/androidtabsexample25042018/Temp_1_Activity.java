/**
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;


import com.example.syneotek001.androidtabsexample25042018.Ui.colorpickerview.dialog.ColorPickerDialog;
import com.example.syneotek001.androidtabsexample25042018.Ui.colorpickerview.view.ColorPanelView;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityTemp1Binding;




public class Temp_1_Activity extends AppCompatActivity implements  View.OnClickListener {


    ActivityTemp1Binding binding;
    Bundle extra;
    String pos = "";
    String mAlignment = "center";
    ColorPanelView mNewColorView;
    int mColor = Color.BLACK, req_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(Temp_1_Activity.this, R.layout.activity_temp_1);

        extra = getIntent().getExtras();

        if (extra != null) {
            pos = extra.getString(Utils.TEMP_POS);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mNewColorView = new ColorPanelView(Temp_1_Activity.this);

        binding.btnCenter.setOnClickListener(this);
        binding.btnRight.setOnClickListener(this);
        binding.btnLeft.setOnClickListener(this);
        mAlignment = "center";
        binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
        binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
        binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
        binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
        binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
        binding.btnRight.setTextColor(getResources().getColor(R.color.black));

        binding.etTxtColor.setText(getString(R.string.black));
        binding.etBgColor.setText(getString(R.string.black));
        binding.btnBgColor.setBackgroundColor(Color.BLACK);
        binding.btnTxtColor.setBackgroundColor(Color.BLACK);

        String[] mlist = getResources().getStringArray(R.array.spin_family);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_list, R.id.spin_item, mlist);
        binding.spinFamily.setAdapter(spinnerArrayAdapter);

        String[] mlist1 = getResources().getStringArray(R.array.spin_weight);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.spinner_list, R.id.spin_item, mlist1);
        binding.spinWeight.setAdapter(spinnerArrayAdapter1);

        binding.etBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_1_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 1), 1);

            }
        });

        binding.etTxtColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivityForResult(new Intent(Temp_1_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 2), 2);
            }
        });

        binding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mTop = binding.etPtop.getText().toString();
                String mRight = binding.etPright.getText().toString();
                String mBottom = binding.etPbottom.getText().toString();
                String mLeft = binding.etPleft.getText().toString();

                mTop = mTop.equals("") ? "0px" : mTop + "px";
                mRight = mRight.equals("") ? "0px" : mRight + "px";
                mBottom = mBottom.equals("") ? "0px" : mBottom + "px";
                mLeft = mLeft.equals("") ? "0px" : mLeft + "px";

                String mPadding = mTop + " " + mRight + " " + mBottom + " " + mLeft;

                String mBg_color = (binding.etBgColor.getText().toString());
                String mTxt_color = (binding.etTxtColor.getText().toString());


                String weight = binding.spinWeight.getSelectedItem().toString();
                String family = binding.spinFamily.getSelectedItem().toString();

                String mData = "<table width=100% bgcolor=\"" + mBg_color + "\"><tr><td><p  data-block-id=\"background\" class=\"title\" " +
                        "style= \"padding: " + mPadding + ";\">" +
                        "<h1 align=\"" + mAlignment + "\" data-block-id=\"main-title\" style=\"font-family: " + family + "; font-weight: " + weight + ";" +
                        " margin: 0px; color: " + mTxt_color + ";\">" +
                        binding.etTitle.getText().toString() + "</h1> <h4 align=\"" + mAlignment + "\" data-block-id=\"sub-title\" style=\"font-family:" +
                        family + "; " + "font-weight: " + weight + "; margin: 0px; " +
                        "color: " + mTxt_color + "\">" + binding.etSubtitle.getText().toString() + "</h4></p></td></tr>";

                LogUtils.i(" mData " + mData);
                Intent i = getIntent();
                i.putExtra(Utils.TEMP_1, mData);
                i.putExtra(Utils.TEMP_POS, pos);
                setResult(0, i);
                finish();
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            mColor = data.getIntExtra(Utils.EXTRA_COLOR, Color.BLACK);
            if (requestCode == 1) {
                binding.etBgColor.setText("#" + Utils.toHex(mColor));
                binding.btnBgColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            } else {
                binding.etTxtColor.setText("#" + Utils.toHex(mColor));
                binding.btnTxtColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            }

        }

    }

    @Override
    protected void onResume() {
        LogUtils.i(" onResume ");
        super.onResume();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_center:
                mAlignment = "center";
                binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_left:
                mAlignment = "left";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.white));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_right:
                mAlignment = "right";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }
}
