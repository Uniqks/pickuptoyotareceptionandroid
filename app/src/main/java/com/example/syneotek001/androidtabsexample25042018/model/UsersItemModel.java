package com.example.syneotek001.androidtabsexample25042018.model;

public class UsersItemModel {
    private String userName = "";
    private String dateCreated = "";
    private String userType = "";

    public UsersItemModel(String userName, String dateCreated, String userType) {
        this.userName = userName;
        this.dateCreated = dateCreated;
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
