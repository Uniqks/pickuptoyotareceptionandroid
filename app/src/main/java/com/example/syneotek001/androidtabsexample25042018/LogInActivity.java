package com.example.syneotek001.androidtabsexample25042018;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.LoginResponse;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;

/*import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;*/

public class LogInActivity extends AppCompatActivity implements ApiResponseInterface {
    private static final String TAG = "LogInActivity";
    EditText edt_email, edt_password;
    Button btn_login;
    TextView txt_forgot_password;
    ApiManager apiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        apiManager = new ApiManager(LogInActivity.this,this);
        edt_email = findViewById(R.id.edtEmail);
        edt_password = findViewById(R.id.edtPassword);
        btn_login = findViewById(R.id.btnLogin);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        init();

        if (Utils.getInstance(this).getBoolean(Utils.PREF_IS_LOGGED_IN) && !Utils.getInstance(this).getString(Utils.PREF_USER_ID).equals("")) {
            startActivity(new Intent(LogInActivity.this,HomeActivity.class)
                    .putExtra(Utils.EXTRA_IS_FROM_NOTIFICATION,false));
            finish();
        }


    }



    private void init() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_email.getText().toString().equals("")) {
                    edt_email.setError("Enter Valid Email.");
                } else if (edt_password.getText().toString().equals("")) {
                    edt_password.setError("Enter Valid Password.");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(edt_email.getText().toString()).matches()) {
                    edt_email.setError("Invalid Pattern");
                } else {


                    apiManager.makeLoginRequest(edt_email.getText().toString(),edt_password.getText().toString());

                    /*String url = "https://pickeringtoyota.com/toyotasalesadmin/webservices/login.php?&email="
                            + edt_email.getText().toString() + "&password=" + edt_password.getText().toString();
                    final KProgressHUD hud = KProgressHUD.create(LogInActivity.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show();
                    Log.e("url", url);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {


                                SharedPreferences preferences = getApplicationContext().getSharedPreferences("status", MODE_PRIVATE);
                                final SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("email", edt_email.getText().toString());
                                editor.putString("password", edt_password.getText().toString());
                                editor.apply();
                                Log.e(TAG, "UserName: " + edt_email.getText().toString());
                                Log.e(TAG, "PAssword: " + edt_password.getText().toString());

                                JSONObject resp = new JSONObject(response);
                                JSONObject object = resp.getJSONObject("data");
                                object.getString("user_id");
                                hud.dismiss();
                                Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                Toast.makeText(LogInActivity.this, "Login Successful.", Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> param = new HashMap<>();
                            param.put("email", edt_email.getText().toString());
                            param.put("password", edt_password.getText().toString());
                            return param;
                        }
                    };
                    Volley.newRequestQueue(getApplicationContext()).add(stringRequest);*/
                }

            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(LogInActivity.this, ForgotPassword.class);
                startActivity(intent);
*/            }
        });
    }

    @Override
    public void isError(String errorCode) {
        LogUtils.i("LogInActivity"+" isError errorCode "+errorCode);
//        {"data":{"user_id":"1","name":"Frank Nava","image":"1528352074_Desert.jpg"},"msg":"successfull login"}
        Utils.getInstance(this).setBoolean(Utils.PREF_IS_LOGGED_IN,true);
        Utils.getInstance(this).setString(Utils.PREF_USER_ID,"1");
        Utils.getInstance(this).setString(Utils.PREF_USER_NAME,"Frank Nava");
        Utils.getInstance(this).setString(Utils.PREF_USER_IMG,"1528352074_Desert.jpg");
        startActivity(new Intent(LogInActivity.this,HomeActivity.class).
                putExtra(Utils.EXTRA_IS_FROM_NOTIFICATION,false));
        finish();
    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        LoginResponse response1 = (LoginResponse) response;
        LogUtils.i("LogInActivity"+" isSuccess User_id "+response1.getData().getUser_id()+" Msg "+response1.getMsg());
        if (!response1.getData().getUser_id().equals("0")){
            Utils.getInstance(this).setBoolean(Utils.PREF_IS_LOGGED_IN,true);
            Utils.getInstance(this).setString(Utils.PREF_USER_ID,response1.getData().getUser_id());
            Utils.getInstance(this).setString(Utils.PREF_USER_NAME,response1.getData().getName());
            Utils.getInstance(this).setString(Utils.PREF_USER_IMG,response1.getData().getImage());
            startActivity(new Intent(LogInActivity.this,HomeActivity.class).
                    putExtra(Utils.EXTRA_IS_FROM_NOTIFICATION,false));
            finish();
        }
    }
}





