package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.UsersItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;
import com.example.syneotek001.androidtabsexample25042018.model.UsersItemModel;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationsFragment extends Fragment implements OnLeadItemViewClickListener,OnBackPressed {

    ArrayList<UsersItemModel> leadItemArray = new ArrayList<>();
    UsersItemAdapter mAdapter;

    public RecyclerView recyclerView;
    public TextView tvNoRecordFound,tvTitle;
    ImageView ivBack;


    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View  view = inflater.inflate(R.layout.fragment_users_list, container, false);
            recyclerView = view.findViewById(R.id.recyclerView);
            tvNoRecordFound = view.findViewById(R.id.tvNoRecordFound);
            tvTitle = view.findViewById(R.id.tvTitle);
            ivBack = view.findViewById(R.id.ivBack);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            });

        ImageView ivMenu;
        ivMenu= view.findViewById(R.id.ivMenu);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).opendrawer();
            }
        });

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.tab_notifications));

        setUpRecyclerView();

        return view;
    }



    private void setUpRecyclerView() {

        leadItemArray.add(new UsersItemModel("Abc Patel", "2018-07-19 02:30 PM", "Sales Man"));
        leadItemArray.add(new UsersItemModel("Xyz test", "2017-12-08 12:54 PM", "Admin"));
        leadItemArray.add(new UsersItemModel("Pqr Shah", "2017-11-28 04:54 AM", "Sales Man"));
        leadItemArray.add(new UsersItemModel("qwerty Panchal", "2017-09-08 06:12 AM", "Admin"));
        leadItemArray.add(new UsersItemModel("Paresh Prajapati", "2017-06-01 07:27 PM", "Sales Man"));
        leadItemArray.add(new UsersItemModel("Raj Sadhu", "2017-03-11 11:47 PM", "Admin"));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new UsersItemAdapter(getActivity(), leadItemArray, this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }



    public void onLeadDetailIconClicked(int position) {
//        ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mContext).mCurrentTab, new LeadDetailFragment(), true, true);
    }

    public void onLeadEmailIconClicked(int position) {
    }

    public void onLeadScheduleIconClicked(int position) {
    }

    public void onLeadInactiveIconClicked(int position) {
    }

    @Override
    public void onLeadLabelClicked(int i) {

    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }


    @Override
    public void onItemClicked(int i) {
        if (leadItemArray.get(i).getUserType().equals("Sales Man")) {
            ChatMessagesFragment chatMessages = new ChatMessagesFragment();
            ArrayList<ChatModel> mCarList = new ArrayList<>();
            mCarList.add(new ChatModel("1", leadItemArray.get(i).getUserName(), "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1513327020_Koala.jpg", 0, true, 1526119235));
            Bundle bundle = new Bundle();
            bundle.putSerializable(ChatMessagesFragment.BUNDLE_CHAT_FRIEND_MODEL, (Serializable) mCarList.get(0));
            chatMessages.setArguments(bundle);
            ((HomeActivity) getActivity()).replaceFragment(chatMessages);
        } else if (leadItemArray.get(i).getUserType().equals("Admin")) {
            ((HomeActivity) getActivity()).replaceFragment(new UserDetailFragment());
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
