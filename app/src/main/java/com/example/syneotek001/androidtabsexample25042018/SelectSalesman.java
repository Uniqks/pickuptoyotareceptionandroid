package com.example.syneotek001.androidtabsexample25042018;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.syneotek001.androidtabsexample25042018.Fragment.SelectSalesmanFragment;

public class SelectSalesman extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_salesman);
        SelectSalesmanFragment fragmentChild = new SelectSalesmanFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragmentChild).commit();
    }
}
