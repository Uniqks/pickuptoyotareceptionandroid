package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;


/**
 * Created by bluegenie-24 on 12/6/18.
 */

public class AdapterPyramidchartLegend extends BaseAdapter {
    Context context;
    String[] labels;
    int[] colors;
    LayoutInflater layoutInflater;

    public AdapterPyramidchartLegend(Context context, String[] labels, int[] colors) {
        this.context = context;
        this.labels = labels;
        this.colors = colors;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return labels.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        convertView = layoutInflater.inflate(R.layout.adapter_pyramidchart_legend, null);

        TextView tv_legend_value = (TextView) convertView.findViewById(R.id.tv_legend_value);
        TextView tvLegendColor = (TextView) convertView.findViewById(R.id.tvLegendColor);
        ImageView iv_legend_shape = (ImageView) convertView.findViewById(R.id.iv_legend_shape);

        tv_legend_value.setText(labels[i]+"");
        tv_legend_value.setTextColor(colors[i]);
        tvLegendColor.setTextColor(colors[i]);
//        iv_legend_shape.setBackgroundColor(colors[i]);

        /*LayerDrawable bgShape = (LayerDrawable) ContextCompat.getDrawable(context,R.drawable.your);
        bgShape.setColor(colors[i]);*/

        /*LayerDrawable drawableFile = (LayerDrawable) context.getResources().getDrawable(R.drawable.bg_pyramidchart_legend_triangle);
        GradientDrawable gradientDrawable = (GradientDrawable) drawableFile.findDrawableByLayerId(R.id.your_shape);
        gradientDrawable.setColor(colors[i]);*/

        iv_legend_shape.setColorFilter(colors[i]);



        return convertView;
    }
}
