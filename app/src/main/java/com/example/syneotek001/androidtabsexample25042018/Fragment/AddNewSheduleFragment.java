package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.ScheduleSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.SelectSalesmanItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.SelectSalesman;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class AddNewSheduleFragment extends Fragment {

    LinearLayout llStartDate, llStartTime,lyt_appoint;
    RelativeLayout lyt_salesman;
    TextView txtStartDate, txtStartTime,tvAdvanced,tv_salesmen,tv_app_type,salesman_name;
    Spinner spinnerLeads;
    ArrayList<String> arr_type = new ArrayList<>();
    ApiManager apiManager;
    int fromlimit = 1;
    Handler handler;
    boolean loadMore = false;
    SelectSalesmanItemAdapter mAdapter;
    ArrayList<SalesmanListResponse.SalesmanListData> mNoteList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoLeadNoteFound;
    String str_salesman_name;
    ImageView ivEditSalesman;
    boolean is_appointment_visible = false, is_salesman_visible = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_shedule_new, container, false);
        ImageView iv_Close = view.findViewById(R.id.ivBack);
        llStartDate = view.findViewById(R.id.llStartDate);
        lyt_appoint= view.findViewById(R.id.lyt_appoint);
        llStartTime = view.findViewById(R.id.llStartTime);
        txtStartDate = view.findViewById(R.id.txtStartDate);
        txtStartTime = view.findViewById(R.id.txtStartTime);
        spinnerLeads = view.findViewById(R.id.spinnerLeads);
        tv_app_type= view.findViewById(R.id.tv_app_type);
        tv_salesmen= view.findViewById(R.id.tv_salesmen);
        ivEditSalesman= view.findViewById(R.id.ivEditSalesman);
        tvAdvanced= view.findViewById(R.id.tvAdvanced);
        salesman_name= view.findViewById(R.id.salesman_name);
        lyt_salesman= view.findViewById(R.id.lyt_salesman);
        arr_type = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_type)));
        initCustomSpinner(arr_type);
        spinnerLeads.setSelection(1);

        tv_app_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(lyt_appoint.getVisibility() == View.VISIBLE) {
                    tv_app_type.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_vector_arrow_right_red,0);
                    lyt_appoint.setVisibility(View.GONE);
                    is_appointment_visible = false;
                }
                else {
                    lyt_appoint.setVisibility(View.VISIBLE);
                    tv_app_type.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_keyboard_arrow_down_white_24dp,0);
                    is_appointment_visible = true;
                }
            }
        });

        llStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year;
                int month;
                int day;
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String monthString = monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;
                                String dayString = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                                txtStartDate.setText(year + "-" + monthString + "-" + dayString);
//                                txtStartDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            }
                        }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        tvAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity) getActivity()).replaceFragment(new AddEditContactMainFragment());
            }
        });

        tv_salesmen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (str_salesman_name != null && !str_salesman_name.equals("")) {

                    if(lyt_salesman.getVisibility() == View.VISIBLE) {
                        lyt_salesman.setVisibility(View.GONE);
                        tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_vector_arrow_right_red,0);
                        is_salesman_visible = false;
                    } else {
                        lyt_salesman.setVisibility(View.VISIBLE);
                        tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_keyboard_arrow_down_white_24dp,0);
                        is_salesman_visible = true;
                    }
                } else {
                    lyt_salesman.setVisibility(View.GONE);
                    tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_vector_arrow_right_red,0);
                    startActivityForResult(new Intent(getActivity(), SelectSalesman.class),100);
                    is_salesman_visible = false;
                }

                /*SelectSalesmanFragment fragmentChild = new SelectSalesmanFragment();
                fragmentChild.setTargetFragment(AddNewSheduleFragment.this, 1);

                ((HomeActivity) getActivity()).replaceFragment(fragmentChild);*/
            }
        });

        ivEditSalesman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), SelectSalesman.class),100);
            }
        });

        llStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    /**
                     * Called when the user is done setting a new time and the dialog has
                     * closed.
                     *
                     * @param view      the view associated with this listener
                     * @param hourOfDay the hour that was set
                     * @param minute    the minute that was set
                     */
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String AM_PM = " AM";
                        if (hourOfDay >= 12) {
                            AM_PM = " PM";
                            if (hourOfDay >= 13 && hourOfDay < 24) {
                                hourOfDay -= 12;
                            } else {
                                hourOfDay = 12;
                            }
                        } else if (hourOfDay == 0) {
                            hourOfDay = 12;
                        }
                        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                        String minuteString = minute < 10 ? "0" + minute : "" + minute;
                        txtStartTime.setText(hourString + ":" + minuteString + " " + AM_PM);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });




        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



            if (requestCode== 100){

                if(data != null)
                {
                    str_salesman_name = data.getStringExtra("sales_man_name");
                    String id = data.getStringExtra("sales_man_id");

                    salesman_name.setText(getString(R.string.str_name)+" : "+str_salesman_name);
                    tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_keyboard_arrow_down_white_24dp,0);
                    lyt_salesman.setVisibility(View.VISIBLE);
                    is_salesman_visible = true;
                }


        }
    }


    private void toast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeadSpinnerAdapter customSpinnerAdapter = new LeadSpinnerAdapter(getActivity(), countries);
        spinnerLeads.setAdapter(customSpinnerAdapter);
        spinnerLeads.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("AddNewSheduleFragment"," onResume is_appointment_visible "+is_appointment_visible+" is_salesman_visible "+is_salesman_visible);

        if (is_appointment_visible) {
            lyt_appoint.setVisibility(View.VISIBLE);
            tv_app_type.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_keyboard_arrow_down_white_24dp,0);
        } else if (!is_appointment_visible) {
            tv_app_type.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_vector_arrow_right_red,0);
            lyt_appoint.setVisibility(View.GONE);
        }

        if (is_salesman_visible) {
            lyt_salesman.setVisibility(View.VISIBLE);
            tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_keyboard_arrow_down_white_24dp,0);
        } else if (!is_salesman_visible) {
            lyt_salesman.setVisibility(View.GONE);
            tv_salesmen.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_vector_arrow_right_red,0);
        }

    }



}
