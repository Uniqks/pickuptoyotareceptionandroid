package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Abhijit Bagal on 09/06/2018.
 */

//============= This Font Utils Class is For Applying font Style To the Text View Text ===============

public class FontUtils {
    public static final String MONT_MED_FONT = "montserrat_medium.ttf";
    public static void applymontserrat_light(TextView textView) {
        try {
            Typeface tf = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/montserrat_light.ttf");
            textView.setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void applymontserrat_medium(TextView textView) {
        try {
            Typeface tf = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/montserrat_medium.ttf");
            textView.setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void applymontserrat_regular(TextView textView) {
        try {
            Typeface tf = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/montserrat_regular.otf");
            textView.setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void applymontserrat_regular_light(TextView textView) {
        try {
            Typeface tf = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/montserrat_regular_light.ttf");
            textView.setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
