package com.example.syneotek001.androidtabsexample25042018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;


public class ItemLeadBinding extends ViewDataBinding {
    @Nullable
    private static final IncludedLayouts sIncludes = null;
    @Nullable
    private static final SparseIntArray sViewsWithIds = new SparseIntArray();
    @NonNull
    public final CardView cvLead;
    @NonNull
    public final ImageView ivEmail;
    @NonNull
    public final ImageView ivInactive;
    @NonNull
    public final ImageView ivInfo;
    @NonNull
    public final ImageView ivLeadLabel;
    @NonNull
    public final ImageView ivSchedule;
    @NonNull
    public final LinearLayout llActionItems;
    @NonNull
    public final LinearLayout llRequest;
    private long mDirtyFlags = -1;
    @NonNull
    private final RelativeLayout mboundView0;
    @NonNull
    public final TextView tvAssignDate;
    @NonNull
    public final TextView tvBusinessRequest;
    @NonNull
    public final TextView tvCustomerId;
    @NonNull
    public final TextView tvDealRequest;
    @NonNull
    public final TextView tvEmail;
    @NonNull
    public final TextView tvLastActivityDate;
    @NonNull
    public final TextView tvName;
    @NonNull
    public final TextView tvPhone;
    @NonNull
    public final TextView tvSourceLink;
    @NonNull
    public final View viewDummySpace;

    static {
        sViewsWithIds.put(R.id.llRequest, 1);
        sViewsWithIds.put(R.id.tvDealRequest, 2);
        sViewsWithIds.put(R.id.tvBusinessRequest, 3);
        sViewsWithIds.put(R.id.cvLead, 4);
        sViewsWithIds.put(R.id.ivLeadLabel, 5);
        sViewsWithIds.put(R.id.tvCustomerId, 6);
        sViewsWithIds.put(R.id.tvName, 7);
        sViewsWithIds.put(R.id.tvAssignDate, 8);
        sViewsWithIds.put(R.id.tvLastActivityDate, 9);
        sViewsWithIds.put(R.id.tvEmail, 10);
        sViewsWithIds.put(R.id.tvPhone, 11);
        sViewsWithIds.put(R.id.tvSourceLink, 12);
        sViewsWithIds.put(R.id.viewDummySpace, 13);
        sViewsWithIds.put(R.id.llActionItems, 14);
        sViewsWithIds.put(R.id.ivInfo, 15);
        sViewsWithIds.put(R.id.ivEmail, 16);
        sViewsWithIds.put(R.id.ivSchedule, 17);
        sViewsWithIds.put(R.id.ivInactive, 18);
    }

    public ItemLeadBinding(@NonNull DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        Object[] bindings = ViewDataBinding.mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds);
        this.cvLead = (CardView) bindings[4];
        this.ivEmail = (ImageView) bindings[16];
        this.ivInactive = (ImageView) bindings[18];
        this.ivInfo = (ImageView) bindings[15];
        this.ivLeadLabel = (ImageView) bindings[5];
        this.ivSchedule = (ImageView) bindings[17];
        this.llActionItems = (LinearLayout) bindings[14];
        this.llRequest = (LinearLayout) bindings[1];
        this.mboundView0 = (RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvAssignDate = (TextView) bindings[8];
        this.tvBusinessRequest = (TextView) bindings[3];
        this.tvCustomerId = (TextView) bindings[6];
        this.tvDealRequest = (TextView) bindings[2];
        this.tvEmail = (TextView) bindings[10];
        this.tvLastActivityDate = (TextView) bindings[9];
        this.tvName = (TextView) bindings[7];
        this.tvPhone = (TextView) bindings[11];
        this.tvSourceLink = (TextView) bindings[12];
        this.viewDummySpace = (View) bindings[13];
        setRootTag(root);
        invalidateAll();
    }

    public void invalidateAll() {
        synchronized (this) {
            this.mDirtyFlags = 1;
        }
        requestRebind();
    }

    public boolean hasPendingBindings() {
        synchronized (this) {
            if (this.mDirtyFlags != 0) {
                return true;
            }
            return false;
        }
    }

    public boolean setVariable(int variableId, @Nullable Object variable) {
        return true;
    }

    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        return false;
    }

    protected void executeBindings() {
        synchronized (this) {
            long dirtyFlags = this.mDirtyFlags;
            this.mDirtyFlags = 0;
        }
    }

    @NonNull
    public static ItemLeadBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ItemLeadBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent bindingComponent) {
        return (ItemLeadBinding) DataBindingUtil.inflate(inflater, R.layout.item_lead, root, attachToRoot, bindingComponent);
    }

    @NonNull
    public static ItemLeadBinding inflate(@NonNull LayoutInflater inflater) {
        return inflate(inflater, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ItemLeadBinding inflate(@NonNull LayoutInflater inflater, @Nullable DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(R.layout.item_lead, null, false), bindingComponent);
    }

    @NonNull
    public static ItemLeadBinding bind(@NonNull View view) {
        return bind(view, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ItemLeadBinding bind(@NonNull View view, @Nullable DataBindingComponent bindingComponent) {
        if ("layout/item_lead_0".equals(view.getTag())) {
            return new ItemLeadBinding(bindingComponent, view);
        }
        throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
    }
}
