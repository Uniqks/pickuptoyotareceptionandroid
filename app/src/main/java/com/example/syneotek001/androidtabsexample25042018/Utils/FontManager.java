package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;


import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.HashMap;

public class FontManager {
    private static FontManager sInstance;
    private HashMap<String, Typeface> mFontCache = new HashMap();

    public static FontManager getInstance() {
        if (sInstance == null) {
            sInstance = new FontManager();
        }
        return sInstance;
    }

    public Typeface getTypeface(@NonNull Context context, @NonNull String path) throws RuntimeException {
        Typeface typeface = (Typeface) this.mFontCache.get(path);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), path);
                this.mFontCache.put(path, typeface);
            } catch (RuntimeException e) {
                throw new RuntimeException("Font assets/" + path + " cannot be loaded");
            }
        }
        return typeface;
    }

    public Typeface getTypeface(@NonNull Context context, int pathResId) throws RuntimeException {
        try {
            return getTypeface(context, context.getResources().getString(pathResId));
        } catch (NotFoundException e) {
            throw new RuntimeException("String resource id " + pathResId + " not found");
        }
    }

    public void applyFont(@NonNull Context context, TextPaint textView, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.VerticalLabelView, 0, 0);
            @SuppressLint("ResourceType") String fontPath = styledAttributes.getString(2);
            if (!TextUtils.isEmpty(fontPath)) {
                Typeface typeface = getTypeface(context, fontPath);
                if (typeface != null) {
                    textView.setTypeface(typeface);
                }
            }
            styledAttributes.recycle();
        }
    }
}
