package com.example.syneotek001.androidtabsexample25042018.Support;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;/*
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;*/
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.BuildConfig;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationIntentService extends IntentService {
    public NotificationIntentService() {
        super("notificationIntentService");
    }

    protected void onHandleIntent(final Intent intent) {
        String action = intent.getAction();
        if (action.equals("left")) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    NotificationManager notificationManager = (NotificationManager) NotificationIntentService.this.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    if (notificationManager != null) {
                        notificationManager.cancel(intent.getIntExtra("notificationId", 0));
                    }
                    NotificationIntentService.this.performAction(NotificationIntentService.this.getBaseContext(), intent.getStringExtra("leadId"), "accept", "Notification Reject");
                }
            });
        } else if (action.equals("right")) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    NotificationManager notificationManager = (NotificationManager) NotificationIntentService.this.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    if (notificationManager != null) {
                        notificationManager.cancel(intent.getIntExtra("notificationId", 0));
                    }
                    NotificationIntentService.this.performAction(NotificationIntentService.this.getBaseContext(), intent.getStringExtra("leadId"), "reject", BuildConfig.FLAVOR);
                }
            });
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    String url = /*"http:" +*/ intent.getStringExtra("Link");
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setData(Uri.parse(url));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    NotificationIntentService.this.getBaseContext().startActivity(i);
                }
            });
        }

    }

    private void performAction(final Context context, String leadId, String action, String rejectMsg) {

        if (action.equals("accept"))
            action = "Accepted";
        else if (action.equals("reject"))
            action = "Rejected";

        Toast.makeText(context,action+" "+getString(R.string.str_clicked),Toast.LENGTH_SHORT).show();



        /*String url;
        if (action.equals("accept")) {
            url = "https://pickeringtoyota.com/toyotasalesadmin/webservices/accept_reject_lead.php?user_id=" + SharedPreference.getUserID(this) + "&lead_id=" + leadId + "&action=" + action;
        } else {
            url = "https://pickeringtoyota.com/toyotasalesadmin/webservices/accept_reject_lead.php?user_id=" + SharedPreference.getUserID(this) + "&lead_id=" + leadId + "&action=" + action + "&reject_msg=" + rejectMsg;
        }
        Log.e("NotificationService", "Url:" + url);
        Volley.newRequestQueue(context).add(new JsonObjectRequest(1, url, new JSONObject(), new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    Log.i("Accept/Reject", "response: " + response.toString());
                    Toast.makeText(context, response.getString("msg"), 0).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.e("Accept/Reject", "Error::" + error.toString());
                Toast.makeText(context, "Error", 0).show();
            }
        }));*/
    }
}
