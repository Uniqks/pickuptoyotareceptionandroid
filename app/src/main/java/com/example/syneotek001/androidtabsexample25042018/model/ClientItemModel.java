package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;

public class ClientItemModel implements Serializable{

    private String clinetType = "";
    private String clientName = "";
    private String clientPhone = "";
    private String clientEmail = "";
    private String clientDescription = "";
    private String clientDate = "";
    private String clientTime = "";
    private boolean isSelected = false;

    public ClientItemModel(String clinetType, String clientName, String clientPhone, String clientEmail, String clientDescription, String clientDate,
                           String clientTime) {
        this.clinetType = clinetType;
        this.clientName = clientName;
        this.clientPhone = clientPhone;
        this.clientEmail = clientEmail;
        this.clientDescription = clientDescription;
        this.clientDate = clientDate;
        this.clientTime = clientTime;
        this.isSelected = false;
    }


    public String getClinetType() {
        return clinetType;
    }

    public void setClinetType(String clinetType) {
        this.clinetType = clinetType;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientDescription() {
        return clientDescription;
    }

    public void setClientDescription(String clientDescription) {
        this.clientDescription = clientDescription;
    }

    public String getClientDate() {
        return clientDate;
    }

    public void setClientDate(String clientDate) {
        this.clientDate = clientDate;
    }

    public String getClientTime() {
        return clientTime;
    }

    public void setClientTime(String clientTime) {
        this.clientTime = clientTime;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
