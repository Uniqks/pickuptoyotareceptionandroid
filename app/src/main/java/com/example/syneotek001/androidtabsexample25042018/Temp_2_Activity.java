/**
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;

import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityTemp2Binding;


public class Temp_2_Activity extends AppCompatActivity {


    ActivityTemp2Binding binding;
    Bundle extra;String pos="";  int mColor = Color.BLACK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(Temp_2_Activity.this,R.layout.activity_temp_2);

        extra = getIntent().getExtras();

        if(extra != null)
        {
            pos = extra.getString(Utils.TEMP_POS);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.etColor.setText(getString(R.string.black));
        binding.etBgColor.setText(getString(R.string.black));
        binding.btnBgColor.setBackgroundColor(Color.BLACK);
        binding.btnTxtColor.setBackgroundColor(Color.BLACK);

        binding.etBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_2_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 1), 1);

            }
        });

        binding.etColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivityForResult(new Intent(Temp_2_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 2), 2);
            }
        });
        binding.seekBorder.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.txtBorder.setText(i+"px");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        binding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String mTop = binding.etPtop.getText().toString();
                String mRight = binding.etPright.getText().toString();
                String mBottom = binding.etPbottom.getText().toString();
                String mLeft = binding.etPleft.getText().toString();

                mTop = mTop.equals("") ?"0px":mTop+"px";
                mRight = mRight.equals("") ?"0px":mRight+"px";
                mBottom = mBottom.equals("") ?"0px":mBottom+"px";
                mLeft = mLeft.equals("") ?"0px":mLeft+"px";

                String mPadding =mTop+" "+mRight+ " "+mBottom+" "+mLeft;


                String mBg_color = (binding.etBgColor.getText().toString());
                String mcolor = (binding.etColor.getText().toString());
                String mSize = binding.seekBorder.getProgress()+"px";
                String mStyle = binding.spinStyle.getSelectedItem().toString();

                    String mData="<table width=100% bgcolor=\"" + mBg_color + "\"><tr><td class=\"divider-simple\" style=\"color:"+binding.etBgColor.getText().toString()+";\"padding: "+mPadding+";\">" +
                            "<table width=\"100%\" cellspacing=\"0\"" +
                            " cellpadding=\"0\" border=\"0\" style=\"border-top: "+mSize+" "+mStyle+" "+mcolor+";\"><tbody><tr><td width=\"100%\"></td></tr>" +
                            "</tbody></table></td></tr></table>";

                    LogUtils.i(" mData " + mData);

                    Intent i = getIntent();
                    i.putExtra(Utils.TEMP_1, mData);
                    i.putExtra(Utils.TEMP_POS, pos);
                    setResult(0, i);
                    finish();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            mColor = data.getIntExtra(Utils.EXTRA_COLOR, Color.BLACK);
            if (requestCode == 1) {
                binding.etBgColor.setText("#" + Utils.toHex(mColor));
                binding.btnBgColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            } else {
                binding.etColor.setText("#" + Utils.toHex(mColor));
                binding.btnTxtColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            }

        }

    }




}
