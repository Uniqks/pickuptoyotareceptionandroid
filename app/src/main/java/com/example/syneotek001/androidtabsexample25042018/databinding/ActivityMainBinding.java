package com.example.syneotek001.androidtabsexample25042018.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.syneotek001.androidtabsexample25042018.R;


public class ActivityMainBinding extends ViewDataBinding {
    @Nullable
    private static final IncludedLayouts sIncludes = new IncludedLayouts(7);
    @Nullable
    private static final SparseIntArray sViewsWithIds = new SparseIntArray();
    @NonNull
    public final FrameLayout container;
    @NonNull
    public final DrawerLayout drawerLayout;
    private long mDirtyFlags = -1;
    @NonNull
    private final LinearLayout mboundView1;
    @Nullable
    private final LayoutDrawerHeaderBinding mboundView11;
    @NonNull
    public final BottomNavigationView navigation;
    @NonNull
    public final NavigationView nvView;
    @NonNull
    public final RecyclerView recyclerView;

    static {
        if (sIncludes != null) {
            sIncludes.setIncludes(1, new String[]{"layout_drawer_header"}, new int[]{2}, new int[]{R.layout.layout_drawer_header});
        }
        sViewsWithIds.put(R.id.container, 3);
        sViewsWithIds.put(R.id.navigation, 4);
        sViewsWithIds.put(R.id.nvView, 5);
        sViewsWithIds.put(R.id.recyclerView, 6);
    }

    private ActivityMainBinding(@NonNull DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 0);
        Object[] bindings = ViewDataBinding.mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds);
        this.container = (FrameLayout) bindings[3];
        this.drawerLayout = (DrawerLayout) bindings[0];
        this.drawerLayout.setTag(null);
        this.mboundView1 = (LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView11 = (LayoutDrawerHeaderBinding) bindings[2];
        setContainedBinding(this.mboundView11);
        this.navigation = (BottomNavigationView) bindings[4];
        this.nvView = (NavigationView) bindings[5];
        this.recyclerView = (RecyclerView) bindings[6];
        setRootTag(root);
        invalidateAll();
    }

    public void invalidateAll() {
        synchronized (this) {
            this.mDirtyFlags = 1;
        }
        if (this.mboundView11 != null) {
            this.mboundView11.invalidateAll();
        }
        requestRebind();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasPendingBindings() {
        /*
        r6 = this;
        r0 = 1;
        monitor-enter(r6);
        r2 = r6.mDirtyFlags;	 Catch:{ all -> 0x0017 }
        r4 = 0;
        r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
        if (r1 == 0) goto L_0x000c;
    L_0x000a:
        monitor-exit(r6);	 Catch:{ all -> 0x0017 }
    L_0x000b:
        return r0;
    L_0x000c:
        monitor-exit(r6);	 Catch:{ all -> 0x0017 }
        r1 = r6.mboundView11;
        r1 = r1.hasPendingBindings();
        if (r1 != 0) goto L_0x000b;
    L_0x0015:
        r0 = 0;
        goto L_0x000b;
    L_0x0017:
        r0 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x0017 }
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pickeringtoyota.databinding.ActivityMainBinding.hasPendingBindings():boolean");
    }

    public boolean setVariable(int variableId, @Nullable Object variable) {
        return true;
    }

    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        return false;
    }

    protected void executeBindings() {
        synchronized (this) {
            long dirtyFlags = this.mDirtyFlags;
            this.mDirtyFlags = 0;
        }
        ViewDataBinding.executeBindingsOn(this.mboundView11);
    }

    @NonNull
    public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent bindingComponent) {
        return (ActivityMainBinding) DataBindingUtil.inflate(inflater, R.layout.activity_main, root, attachToRoot, bindingComponent);
    }

    @NonNull
    public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater) {
        return inflate(inflater, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ActivityMainBinding inflate(@NonNull LayoutInflater inflater, @Nullable DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(R.layout.activity_main, null, false), bindingComponent);
    }

    @NonNull
    public static ActivityMainBinding bind(@NonNull View view) {
        return bind(view, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    public static ActivityMainBinding bind(@NonNull View view, @Nullable DataBindingComponent bindingComponent) {
        if ("layout/activity_main_0".equals(view.getTag())) {
            return new ActivityMainBinding(bindingComponent, view);
        }
        throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
    }
}
