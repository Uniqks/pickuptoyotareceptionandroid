package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadSoldInactiveItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.UpdateLeadStatusDialog;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.HeaderItemDecoration;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;

public class SoldInactiveLeadListFragment extends Fragment implements OnLeadItemViewClickListener,OnBackPressed {
    static final /* synthetic */ boolean $assertionsDisabled = (!SoldInactiveLeadListFragment.class.desiredAssertionStatus());
    public static final String BUNDLE_LEAD_STATUS_TYPE = "lead_status_type";
    public static final String BUNDLE_UPDATE_STATUS_POSITION = "position";
    public static final String BUNDLE_UPDATE_STATUS_VALUE = "statusValue";
    public static final int LEAD_LIST_UPDATE_STATUS = 1;
    ArrayList<LeadItemModel> leadItemArray = new ArrayList<>();
    LeadSoldInactiveItemAdapter mAdapter;

    public RecyclerView recyclerView;
    public TextView tvNoRecordFound,tvTitle;
    ImageView ivBack;


    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View  view = inflater.inflate(R.layout.fragment_sold_inactive_lead_list, container, false);
            recyclerView = view.findViewById(R.id.recyclerView);
            tvNoRecordFound = view.findViewById(R.id.tvNoRecordFound);
            tvTitle = view.findViewById(R.id.tvTitle);
            ivBack = view.findViewById(R.id.ivBack);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            });
            prepareLayouts(getArguments());
        return view;
    }

    private void prepareLayouts(Bundle bundle) {
        if (bundle != null) {
            String leadStatusType = bundle.getString(BUNDLE_LEAD_STATUS_TYPE);
            if (!$assertionsDisabled && leadStatusType == null) {
                throw new AssertionError();
            } else if (leadStatusType.equals(getResources().getString(R.string.sold_leads))) {
//                setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.sold_leads));
                tvTitle.setText(getResources().getString(R.string.sold_leads));
                setUpRecyclerView(leadStatusType);
                return;
            } else if (leadStatusType.equals(getResources().getString(R.string.inactive_dead_leads))) {
//                setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.inactive_dead_leads));
                tvTitle.setText(getResources().getString(R.string.inactive_dead_leads));
                setUpRecyclerView(leadStatusType);
                return;
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
                return;
            }
        }
        Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    private void setUpRecyclerView(String leadStatusType) {
        if (leadStatusType.equals(getResources().getString(R.string.sold_leads))) {
            leadItemArray.add(new LeadItemModel("1", "152244893677", "1", "Abc Patel", "abc@gmail.com", "9876655443", "www.google.com", "5", 1486961877, 1503061200, 0, 0));
            leadItemArray.add(new LeadItemModel("2", "152244893477", "2", "Xyz test", "xyz@gmail.com", "9878965443", "www.gmail.com", "5", 1486234877, 1508751200, 0, 0));
            leadItemArray.add(new LeadItemModel("3", "152244893489", "3", "Pqr Shah", "pqr@gmail.com", "9879665987", "www.youtube.com", "5", 1486904400, 1490101200, 0, 0));
            leadItemArray.add(new LeadItemModel("4", "152244891563", "4", "qwerty Panchal", "qwerty@gmail.com", "9978965989", "www.drive.com", "5", 1515243600, 1524754800, 0, 0));
            leadItemArray.add(new LeadItemModel("5", "152244891123", "5", "Paresh Prajapati", "paresh@gmail.com", "9978966598", "www.bitbucket.com", "5", 1512918000, 1518894000, 0, 0));
            leadItemArray.add(new LeadItemModel("6", "152244891123", "1", "Raj Sadhu", "raj@gmail.com", "9978912345", "www.test.com", "5", 1461492000, 1464955200, 0, 0));
        } else {
            if (leadStatusType.equals(getResources().getString(R.string.inactive_dead_leads))) {
                leadItemArray.add(new LeadItemModel("1", "152244893677", "1", "Abc Patel", "abc@gmail.com", "9876655443", "www.google.com", "6", 1486961877, 1503061200, 0, 0));
                leadItemArray.add(new LeadItemModel("2", "152244893477", "2", "Xyz test", "xyz@gmail.com", "9878965443", "www.gmail.com", "6", 1486234877, 1508751200, 0, 0));
                leadItemArray.add(new LeadItemModel("3", "152244893489", "3", "Pqr Shah", "pqr@gmail.com", "9879665987", "www.youtube.com", "6", 1486904400, 1490101200, 0, 0));
                leadItemArray.add(new LeadItemModel("4", "152244891563", "4", "qwerty Panchal", "qwerty@gmail.com", "9978965989", "www.drive.com", "6", 1515243600, 1524754800, 0, 0));
                leadItemArray.add(new LeadItemModel("5", "152244891123", "5", "Paresh Prajapati", "paresh@gmail.com", "9978966598", "www.bitbucket.com", "6", 1512918000, 1518894000, 0, 0));
                leadItemArray.add(new LeadItemModel("6", "152244891123", "1", "Raj Sadhu", "raj@gmail.com", "9978912345", "www.test.com", "6", 1461492000, 1464955200, 0, 0));
            }
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new LeadSoldInactiveItemAdapter(getActivity(), leadItemArray, this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void onLeadLabelClicked(int position) {
        UpdateLeadStatusDialog dialog = new UpdateLeadStatusDialog();
        Bundle args = new Bundle();
        args.putString(BUNDLE_UPDATE_STATUS_VALUE, ((LeadItemModel) this.leadItemArray.get(position)).getLeadStatus());
        args.putInt("position", position);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 1);
        dialog.show(getFragmentManager().beginTransaction(), UpdateLeadStatusDialog.class.getSimpleName());
    }

    public void onLeadDetailIconClicked(int position) {
//        ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mContext).mCurrentTab, new LeadDetailFragment(), true, true);
    }

    public void onLeadEmailIconClicked(int position) {
    }

    public void onLeadScheduleIconClicked(int position) {
    }

    public void onLeadInactiveIconClicked(int position) {
    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        String newStatus = bundle.getString(BUNDLE_UPDATE_STATUS_VALUE);
                        int position = bundle.getInt("position");
                        LeadItemModel objModel = (LeadItemModel) leadItemArray.get(position);
                        objModel.setLeadStatus(newStatus);
                        mAdapter.notifyItemChanged(position, objModel);
                        return;
                    }
                    return;
                } else if (resultCode == 0) {
                    Logger.m6e("PICKER", "Result Cancel");
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @Override
    public void onItemClicked(int i) {

    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
