package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.syneotek001.androidtabsexample25042018.Fragment.COROLLAFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.ComposeFragment;

/**
 * Created by Synotek on 27/06/2018.
 */

public class CamaryPagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;

    public CamaryPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new COROLLAFragment();
        } else if (position == 1) {
            fragment = new ComposeFragment();
        } else if (position == 2) {
            fragment = new ComposeFragment();
        }
        return fragment;
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
   /* @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
           // title = "EXTERIOR ACCESSORIES";
        }
        else if (position == 1)
        {
            //title = "INTERIOR ACCESSORIES";
        }


        return title;
    }*/
}