package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.TaskItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class TaskItemAdapter extends RecyclerView.Adapter<TaskItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<TaskItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (TaskItemAdapter.this.onItemClickListener != null) {
                    TaskItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (TaskItemAdapter.this.onItemClickListener != null) {
                    TaskItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivEdit:
                if (TaskItemAdapter.this.onItemClickListener != null) {
                    TaskItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivSchedule:
                if (TaskItemAdapter.this.onItemClickListener != null) {
                    TaskItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEdit, ivDelete, ivInfo;
        LinearLayout llActionItems;
        LinearLayout ll_Category, ll_TaskStatus, ll_TaskDateTime, ll_TaskNotes;
        TextView tvTaskStatus, tvTaskCategory, tvName, tvTaskDateTime, tvTaskNotes,tvHeadTaskTitle,tvTaskTitle;
        Button btnStatusFollowUp;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivInfo = view.findViewById(R.id.ivInfo);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvTaskStatus = view.findViewById(R.id.tvTaskStatus);
            tvTaskCategory = view.findViewById(R.id.tvTaskCategory);
            tvName = view.findViewById(R.id.tvName);
            tvTaskDateTime = view.findViewById(R.id.tvTaskDateTime);
            tvTaskNotes = view.findViewById(R.id.tvTaskNotes);
            tvHeadTaskTitle = view.findViewById(R.id.tvHeadTaskTitle);
            tvTaskTitle = view.findViewById(R.id.tvTaskTitle);

            ll_Category = view.findViewById(R.id.ll_Category);
            ll_TaskStatus = view.findViewById(R.id.ll_TaskStatus);
            ll_TaskDateTime = view.findViewById(R.id.ll_TaskDateTime);
            ll_TaskNotes = view.findViewById(R.id.ll_TaskNotes);
            btnStatusFollowUp = view.findViewById(R.id.btnStatusFollowUp);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public TaskItemAdapter(Context context, ArrayList<TaskItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_task, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        TaskItemModel mLeadItem = (TaskItemModel) mData.get(position);
        holder.tvTaskCategory.setText(mLeadItem.getTask_category());
        holder.tvTaskDateTime.setText(mLeadItem.getTask_date());
        holder.tvTaskNotes.setText(mLeadItem.getTask_notes());
        holder.tvHeadTaskTitle.setText(mLeadItem.getTask_title());
        holder.tvTaskTitle.setText(mLeadItem.getTask_title());
        String taskStatus = mLeadItem.getTask_status();

        if (taskStatus.equals("1")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_pending));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_pending));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_pending);
        } else if (taskStatus.equals("2")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_not_reply));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_not_reply));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_not_reply);
        } else if (taskStatus.equals("3")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_follow_up));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_follow_up));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_follow_up);
        } else if (taskStatus.equals("4")) {
            holder.btnStatusFollowUp.setText(mContext.getString(R.string.str_task_status_completed));
            holder.tvTaskStatus.setText(mContext.getString(R.string.str_task_status_completed));
            holder.btnStatusFollowUp.setBackgroundResource(R.drawable.bg_task_status_completed);
        }

        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.ll_Category);
            viewList.add(holder.ll_TaskStatus);
            viewList.add(holder.ll_TaskDateTime);
            viewList.add((holder.ll_TaskNotes));
            viewList.add((holder.btnStatusFollowUp));
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.ll_Category);
        viewList.add(holder.ll_TaskStatus);
        viewList.add(holder.ll_TaskDateTime);
        viewList.add(holder.ll_TaskNotes);
        viewList.add(holder.btnStatusFollowUp);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    TaskItemModel mLeadItem = (TaskItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });



        holder.ivInfo.setOnClickListener(this);
        holder.ivEdit.setOnClickListener(this);
        holder.ivDelete.setOnClickListener(this);


    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
