package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.Adapter.LeaseSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentEditClientBinding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentEditScheduleBinding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ClientItemModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class EditScheduleFragment extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
    FragmentEditScheduleBinding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_schedule, container, false);
        View view = binding.getRoot();

 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblName);
        setAnimation(binding.lblEmail);
        setAnimation(binding.lblMobileNumber);
        setAnimation(binding.lblPassword);
        setAnimation(binding.lblAddress);

        setAnimation(binding.etName);
        setAnimation(binding.etEmail);
        setAnimation(binding.etMobile);
        setAnimation(binding.etPassword);
        setAnimation(binding.etAddress);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_client_type)));
        initCustomSpinner(arr_salesman_status);

        binding.btnAdvanced.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).replaceFragment(new AddEditContactMainFragment());
            }
        });


        if (getArguments() != null) {

            binding.llDate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int year;
                    int month;
                    int day;
                    final Calendar c = Calendar.getInstance();
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    binding.txtDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                }
                            }, year, month, day);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });


            binding.llTime.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        /**
                         * Called when the user is done setting a new time and the dialog has
                         * closed.
                         *
                         * @param view      the view associated with this listener
                         * @param hourOfDay the hour that was set
                         * @param minute    the minute that was set
                         */
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            binding.txtTime.setText(hourOfDay + ":" + minute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            if (getArguments().getSerializable("client_profile") != null) {
                ClientItemModel ClientItemModel = (ClientItemModel) getArguments().getSerializable("client_profile");
                if (ClientItemModel.getClinetType().equals("1")) {
                    binding.spinnerStatus.setSelection(0);
                } else if (ClientItemModel.getClinetType().equals("2")) {
                    binding.spinnerStatus.setSelection(1);
                } else if (ClientItemModel.getClinetType().equals("3")) {
                    binding.spinnerStatus.setSelection(2);
                } else if (ClientItemModel.getClinetType().equals("4")) {
                    binding.spinnerStatus.setSelection(3);
                }

                binding.etName.setText(ClientItemModel.getClientName());
                binding.etEmail.setText(ClientItemModel.getClientEmail());
                binding.etMobile.setText(ClientItemModel.getClientPhone());
                binding.etDescription.setText(ClientItemModel.getClientDescription());
                binding.txtDate.setText(ClientItemModel.getClientDate());
                binding.txtTime.setText(ClientItemModel.getClientTime());
            }

            if (getArguments().getBoolean("is_edit")) {
                enableDisableFields(true);
                binding.tvTitlemain.setText(getString(R.string.str_edit_schedule));
                binding.btnSaveAndNext.setVisibility(View.VISIBLE);
            } else {
               enableDisableFields(false);
                binding.tvTitlemain.setText(getString(R.string.str_details_client));
                binding.btnSaveAndNext.setVisibility(View.GONE);
            }
        }


        return view;
    }

    public void enableDisableFields(boolean is_enabled) {

        if (!is_enabled) {
            binding.etName.setHintTextColor(ContextCompat.getColor(getActivity(),R.color.black));
        }

        binding.etName.setFocusable(is_enabled);
        binding.etEmail.setFocusable(is_enabled);
        binding.etMobile.setFocusable(is_enabled);
        binding.etDescription.setFocusable(is_enabled);
        binding.llDate.setEnabled(is_enabled);
        binding.llTime.setEnabled(is_enabled);
        binding.spinnerStatus.setEnabled(is_enabled);
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeaseSpinnerAdapter customSpinnerAdapter = new LeaseSpinnerAdapter(getActivity(), countries);
        binding.spinnerStatus.setAdapter(customSpinnerAdapter);
        binding.spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                onBackPressed();
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
