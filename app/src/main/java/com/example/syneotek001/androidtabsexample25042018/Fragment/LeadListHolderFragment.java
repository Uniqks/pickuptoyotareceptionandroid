package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

public class LeadListHolderFragment extends Fragment implements OnBackPressed{
    FragmentTabsPagerAdapter mAdapter;
    FragmentTabListHolderBinding mBinding;
    View view;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    Activity activity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view=inflater.inflate(R.layout.fragment_tab_list_holder, container, false);
//        activity = getActivity();
//        ((HomeActivity)activity).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) activity));
        // Inflate the layout for this fragment
        tabLayout=view.findViewById(R.id.tabLayout);
        viewPager=view.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        setHasOptionsMenu(true);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        ImageView ivfilter=view.findViewById(R.id.ivfilter);
        ivfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).replaceFragment(new LeadFilterFragment());
            }
        });
        return view;
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                tab.setCustomView(llTab);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
//        mAdapter = new FragmentTabsPagerAdapter(getFragmentManager());
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(new LeadListFragment(), getResources().getString(R.string.new_lead));
        mAdapter.addFragment(new LeadListFragment(), getResources().getString(R.string.follow_up_lead));
        mAdapter.addFragment(new LeadListFragment(), getResources().getString(R.string.under_contract_lead));
        mAdapter.addFragment(new LeadListFragment(), getResources().getString(R.string.delivery));
        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lead_management, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
//                ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mContext).mCurrentTab, new LeadFilterFragment(), true, true);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
    }
}
