package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.graphics.Color;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterLinechartLegend;
import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterViewSchedule;
import com.example.syneotek001.androidtabsexample25042018.Adapter.ScheduleSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.DateTimeInterpreter;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.MonthLoader;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.WeekView;
import com.example.syneotek001.androidtabsexample25042018.Ui.weekview.WeekViewEvent;
import com.example.syneotek001.androidtabsexample25042018.Utils.ExpandableHeightGridView;
import com.example.syneotek001.androidtabsexample25042018.Utils.NonScrollListView;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ClientItemModel;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.github.sundeepk.compactcalendarview.domain.Event.EventData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


public class FragmentReports extends ReportsLineChartBaseFragment implements  AdapterViewSchedule.TaskClick {

    ImageView ivAdd, ivBack;

    private static final String TAG = "ViewTaskFragment";
    TextView tvDate, tvTitle, tvSelectedDate;
    ImageView ivPrevious, ivNext;
    final ArrayList<EventData> arrTasks = new ArrayList<>();
    AdapterViewSchedule adapter;
    Spinner spinnerScheduleCategory;
    ArrayList<String> arr_schedule_categories = new ArrayList<>();

    public LineChart mChart;
    ArrayList<LineGraphDataModel> leadsEntry;

    String[] arr_linechart_labels = new String[11];
    int[] arr_linechart_colors = new int[11];

    ExpandableHeightGridView gv_linechart_legend;

    public FragmentReports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_reports, container, false);

        // Inflate the layout for this fragment
        // Get a reference for the week view in the layout.
        tvTitle = view.findViewById(R.id.tvTitle);
        ivAdd = view.findViewById(R.id.ivAdd);
        ivBack = view.findViewById(R.id.ivBack);
        mChart = view.findViewById(R.id.mChart);
        gv_linechart_legend = view.findViewById(R.id.gv_linechart_legend);
        setBinding(mChart);
        prepareGraphLayout(3, 800.0f);

        spinnerScheduleCategory = view.findViewById(R.id.spinnerScheduleCategory);
//        arr_schedule_categories = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_schedule_categories)));



        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).replaceFragment(new AddNewSheduleFragment());
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        tvTitle.setText(getString(R.string.str_reports));


        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.

        tvSelectedDate = view.findViewById(R.id.tvSelectedDate);
        tvDate = view.findViewById(R.id.tvDate);
        ivPrevious = view.findViewById(R.id.ivPrevious);
        ivNext = view.findViewById(R.id.ivNext);

        final NonScrollListView bookingsListView = view.findViewById(R.id.bookings_listview);
//        final ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, mutableBookings);
//        bookingsListView.setAdapter(adapter);

        Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 1);
        startTime.add(Calendar.DAY_OF_WEEK, 1);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.MONTH, startTime.get(Calendar.MONTH) - 1);
        startTime.set(Calendar.YEAR, startTime.get(Calendar.YEAR));

        Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR, 2);
        endTime.add(Calendar.DAY_OF_WEEK, 1);
        endTime.set(Calendar.MONTH, endTime.get(Calendar.MONTH) - 1);

        Calendar startTime1 = Calendar.getInstance();
        startTime1.set(Calendar.HOUR_OF_DAY, 1);
        startTime1.add(Calendar.DAY_OF_WEEK, 1);
        startTime1.set(Calendar.MINUTE, 0);
        startTime1.set(Calendar.MONTH, startTime1.get(Calendar.MONTH) - 1);
        startTime1.set(Calendar.YEAR, startTime1.get(Calendar.YEAR));

        Calendar endTime1 = (Calendar) startTime1.clone();
        endTime1.add(Calendar.HOUR, 4);
        endTime.add(Calendar.DAY_OF_WEEK, 2);
        endTime1.set(Calendar.MONTH, endTime1.get(Calendar.MONTH) - 1);

        Calendar startTime2 = Calendar.getInstance();
        startTime2.set(Calendar.HOUR_OF_DAY, 1);
        startTime2.add(Calendar.DAY_OF_WEEK, 1);
        startTime2.set(Calendar.MINUTE, 0);
        startTime2.set(Calendar.MONTH, startTime2.get(Calendar.MONTH) - 1);
        startTime2.set(Calendar.YEAR, startTime2.get(Calendar.YEAR));

        Calendar endTime2 = (Calendar) startTime2.clone();
        endTime2.add(Calendar.HOUR, 5);
        endTime.add(Calendar.DAY_OF_WEEK, 5);
        endTime2.set(Calendar.MONTH, endTime2.get(Calendar.MONTH) - 1);

        Calendar startTime3 = Calendar.getInstance();
        startTime3.set(Calendar.HOUR_OF_DAY, 1);
        startTime3.add(Calendar.DAY_OF_WEEK, 1);
        startTime3.set(Calendar.MINUTE, 0);
        startTime3.set(Calendar.MONTH, startTime3.get(Calendar.MONTH) - 1);
        startTime3.set(Calendar.YEAR, startTime3.get(Calendar.YEAR));

        Calendar endTime3 = (Calendar) startTime3.clone();
        endTime3.add(Calendar.HOUR, 7);
        endTime3.add(Calendar.DAY_OF_WEEK, 7);
        endTime3.set(Calendar.MONTH, endTime3.get(Calendar.MONTH) - 1);

        arrTasks.add(new Event().new EventData("Test Appointment 1",""+new Date(startTime.getTimeInMillis()),""+new Date(endTime.getTimeInMillis())));
        arrTasks.add(new Event().new EventData("Test Appointment 2",""+new Date(startTime1.getTimeInMillis()),""+new Date(endTime1.getTimeInMillis())));
        arrTasks.add(new Event().new EventData("Test Appointment 3",""+new Date(startTime2.getTimeInMillis()),""+new Date(endTime2.getTimeInMillis())));
        arrTasks.add(new Event().new EventData("Test Appointment 4",""+new Date(startTime3.getTimeInMillis()),""+new Date(endTime3.getTimeInMillis())));


        adapter = new AdapterViewSchedule(getActivity(), arrTasks);
        adapter.setTaskClick(FragmentReports.this);
        bookingsListView.setAdapter(adapter);

        Calendar startTime5 = Calendar.getInstance();
        startTime5.set(Calendar.MONTH, startTime5.get(Calendar.MONTH));
        Calendar startTime6 = Calendar.getInstance();
        startTime6.set(Calendar.MONTH, startTime6.get(Calendar.MONTH)+1);
        Calendar startTime9 = Calendar.getInstance();
        startTime9.set(Calendar.MONTH, startTime9.get(Calendar.MONTH)+2);
        Calendar startTime12 = Calendar.getInstance();
        startTime12.set(Calendar.MONTH, startTime12.get(Calendar.MONTH)+3);

        /*if (arrTasks.get(position).getTaskStartTime()!=null && !arrTasks.get(position).getTaskStartTime().equals("")
                && arrTasks.get(position).getTaskEndTime()!=null && !arrTasks.get(position).getTaskEndTime().equals(""))*/{
            SimpleDateFormat src = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            SimpleDateFormat dest = new SimpleDateFormat("MMM yyyy");
            Date startTime4 = null;
            Date startTime7 = null;
            Date startTime8 = null;
            Date startTime10 = null;
            Date startTime11 = null;
            try {
                startTime4 = src.parse(arrTasks.get(0).getTaskStartTime());
                startTime7 = src.parse(String.valueOf(new Date(startTime5.getTimeInMillis())));
                startTime8 = src.parse(String.valueOf(new Date(startTime6.getTimeInMillis())));
                startTime10 = src.parse(String.valueOf(new Date(startTime9.getTimeInMillis())));
                startTime11 = src.parse(String.valueOf(new Date(startTime12.getTimeInMillis())));
            } catch (ParseException e) {
                //handle exception
            }
            String strStartTime = dest.format(startTime4);

            arr_schedule_categories.add("All");
            arr_schedule_categories.add(""+startTime12.get(Calendar.YEAR));
            arr_schedule_categories.add(dest.format(startTime11)+" (future)");
            arr_schedule_categories.add(dest.format(startTime10)+" (future)");
            arr_schedule_categories.add(dest.format(startTime8)+" (future)");
            arr_schedule_categories.add(dest.format(startTime7));
            arr_schedule_categories.add(strStartTime);
            initCustomSpinner(arr_schedule_categories);
            spinnerScheduleCategory.setSelection(5);

        }

        return view;
    }

    private void prepareGraphLayout(int count, float range) {
        ArrayList<Entry> newLeads = new ArrayList<>();
        ArrayList<Entry> followUpLeads = new ArrayList<>();
        ArrayList<Entry> underContractLeads = new ArrayList<>();
        ArrayList<Entry> deliveryLeads = new ArrayList<>();
        ArrayList<Entry> soldLeads = new ArrayList<>();
        ArrayList<Entry> inactiveLeads = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            newLeads.add(new Entry((float) i, ((float) (Math.random() * ((double) range))) + 3.0f, getResources().getDrawable(R.drawable.ic_chart_marker_yellow)));
            followUpLeads.add(new Entry((float) i, 20.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_orange)));
            underContractLeads.add(new Entry((float) i, 50.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_sky)));
            deliveryLeads.add(new Entry((float) i, 30.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_green)));
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
            inactiveLeads.add(new Entry((float) i, 25.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_red)));
        }
        this.leadsEntry = new ArrayList<>();
        this.leadsEntry.add(new LineGraphDataModel("Total Calls", ContextCompat.getColor(this.mContext, R.color.yellow_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_yellow), ContextCompat.getColor(this.mContext, R.color.yellow_opacity_50), newLeads));
//        this.leadsEntry.add(new LineGraphDataModel("Total Walk-Ins", ContextCompat.getColor(this.mContext, R.color.orange_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_orange), ContextCompat.getColor(this.mContext, R.color.orange_opacity_50), followUpLeads));
        this.leadsEntry.add(new LineGraphDataModel("Total Walk-Ins", ContextCompat.getColor(this.mContext, R.color.green_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_green), ContextCompat.getColor(this.mContext, R.color.green_opacity_50), deliveryLeads));
        this.leadsEntry.add(new LineGraphDataModel("Total Leads", ContextCompat.getColor(this.mContext, R.color.sky_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_sky), ContextCompat.getColor(this.mContext, R.color.sky_opacity_50), underContractLeads));

//        this.leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(this.mContext, R.color.pink_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_pink), ContextCompat.getColor(this.mContext, R.color.pink_opacity_50), soldLeads));
//        this.leadsEntry.add(new LineGraphDataModel("Total Leads", ContextCompat.getColor(this.mContext, R.color.red_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_red), ContextCompat.getColor(this.mContext, R.color.red_opacity_50), inactiveLeads));
        setUpChart(2010, count + 2010, this.leadsEntry);

        for (int i = 0; i < leadsEntry.size(); i++) {
            arr_linechart_labels[i] = leadsEntry.get(i).getTitle();
            arr_linechart_colors[i] = leadsEntry.get(i).getColorOpacity80();
        }

        AdapterLinechartLegend adapterLegend = new AdapterLinechartLegend(getActivity(), arr_linechart_labels, arr_linechart_colors);
        gv_linechart_legend.setAdapter(adapterLegend);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void initCustomSpinner(ArrayList<String> countries) {

        ScheduleSpinnerAdapter customSpinnerAdapter = new ScheduleSpinnerAdapter(getActivity(), countries);
        spinnerScheduleCategory.setAdapter(customSpinnerAdapter);
        spinnerScheduleCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onTaskClick(int position) {
        /*for (int i=0;i<arrTasks.size();i++) {
            if (i!=position)
                arrTasks.get(i).setIs_selected(false);
        }
        arrTasks.get(position).setIs_selected(true);
        adapter.notifyDataSetChanged();*/


        String dummy_descripton = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";

        EditScheduleFragment editSalesmanFragment = new EditScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("client_profile", new ClientItemModel("1", "Abc Patel", "9876655443", "abc@gmail.com", dummy_descripton, "2018-08-28", "13:44"));
        bundle.putBoolean("is_edit", true);
        editSalesmanFragment.setArguments(bundle);
        ((HomeActivity) getActivity()).replaceFragment(editSalesmanFragment);

//        ((HomeActivity) getActivity()).replaceFragment(new ViewScheduleFragment());

    }


    /*@Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }*/

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
