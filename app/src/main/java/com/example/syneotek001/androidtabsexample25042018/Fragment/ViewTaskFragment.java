package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterViewTask;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.github.sundeepk.compactcalendarview.domain.Event.EventData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class ViewTaskFragment extends Fragment implements OnBackPressed, AdapterViewTask.TaskClick {

    CompactCalendarView compactCalendarView;
    private static final String TAG = "ViewTaskFragment";
    TextView tvDate,tvTitle,tvSelectedDate;
    ImageView ivPrevious,ivNext;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth1 = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    private boolean shouldShow = false;
    final ArrayList<String> mutableBookings = new ArrayList<>();
    final ArrayList<EventData> arrTasks = new ArrayList<>();
    AdapterViewTask adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_viewtask, container, false);
        ImageView ivBack = view.findViewById(R.id.iv_back);
        ImageView ivAdd = view.findViewById(R.id.ivAdd);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        tvTitle = view.findViewById(R.id.tvTitle);
        tvSelectedDate = view.findViewById(R.id.tvSelectedDate);
        tvDate = view.findViewById(R.id.tvDate);
        ivPrevious = view.findViewById(R.id.ivPrevious);
        ivNext = view.findViewById(R.id.ivNext);
        tvTitle.setText(getString(R.string.str_view_task));

        final ListView bookingsListView = view.findViewById(R.id.bookings_listview);
//        final ArrayAdapter adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, mutableBookings);
//        bookingsListView.setAdapter(adapter);

        adapter = new AdapterViewTask(getActivity(),arrTasks);
        adapter.setTaskClick(ViewTaskFragment.this);
        bookingsListView.setAdapter(adapter);

        compactCalendarView = view.findViewById(R.id.compactcalendar_view);

        compactCalendarView.setUseThreeLetterAbbreviation(false);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.setIsRtl(false);
        compactCalendarView.displayOtherMonthDays(false);

        loadEvents();
        loadEventsForYear(2018);
        compactCalendarView.invalidate();

        logEventsByMonth(compactCalendarView);

        tvDate.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()).toUpperCase());
        tvSelectedDate.setText(dateFormatForMonth1.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                onDateClick(dateClicked);

                /*tvDate.setText(dateFormatForMonth.format(dateClicked).toUpperCase());
                tvSelectedDate.setText(dateFormatForMonth1.format(dateClicked));
                List<Event> bookingsFromMap = compactCalendarView.getEvents(dateClicked);
                Log.d(TAG, "inside onclick " + dateFormatForDisplaying.format(dateClicked));
                if (bookingsFromMap != null) {
                    Log.d(TAG, bookingsFromMap.toString());
//                    mutableBookings.clear();
                    arrTasks.clear();
                    for (Event booking : bookingsFromMap) {
//                        mutableBookings.add((String) booking.getData());
                        arrTasks.add(booking.getEventData());
                    }
                    adapter.notifyDataSetChanged();
                }*/

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                tvDate.setText(dateFormatForMonth.format(firstDayOfNewMonth).toUpperCase());
                tvSelectedDate.setText(dateFormatForMonth1.format(firstDayOfNewMonth));
            }
        });

        ivPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollLeft();
            }
        });

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.scrollRight();
            }
        });

        onDateClick(new Date());

        return view;
    }

    public void onDateClick(Date dateClicked) {
        tvDate.setText(dateFormatForMonth.format(dateClicked).toUpperCase());
        tvSelectedDate.setText(dateFormatForMonth1.format(dateClicked));
        List<Event> bookingsFromMap = compactCalendarView.getEvents(dateClicked);
        Log.d(TAG, "inside onclick " + dateFormatForDisplaying.format(dateClicked));
        if (bookingsFromMap != null) {
            Log.d(TAG, bookingsFromMap.toString());
//                    mutableBookings.clear();
            arrTasks.clear();
            for (Event booking : bookingsFromMap) {
//                        mutableBookings.add((String) booking.getData());
                arrTasks.add(booking.getEventData());
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        Log.d(TAG, "Events for Aug with simple date formatter: " + dates);
        Log.d(TAG, "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }

    private void loadEvents() {
        addEvents(-1, -1);
        addEvents(Calendar.DECEMBER, -1);
        addEvents(Calendar.AUGUST, -1);
    }

    private void loadEventsForYear(int year) {
        addEvents(Calendar.DECEMBER, year);
        addEvents(Calendar.AUGUST, year);
    }

    private void addEvents(int month, int year) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            if (year > -1) {
                currentCalender.set(Calendar.ERA, GregorianCalendar.AD);
                currentCalender.set(Calendar.YEAR, year);
            }
            currentCalender.add(Calendar.DATE, i);
//            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
        }

        List<Event> events = getEvents(new Date().getTime(), 25);

        compactCalendarView.addEvents(events);

    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"John Kumar","male","1",false)));
        } else if ( day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Jansi Rani","female","2",false)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Aarya","male","3",false)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Roja","female","1",false)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Kiruba","male","2",false)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis),new Event().new EventData(""+new Date(timeInMillis),"Shalini","female","3",false)));
        }
    }

    public void replaceFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onTaskClick(int position) {
        for (int i=0;i<arrTasks.size();i++) {
            if (i!=position)
                arrTasks.get(i).setIs_selected(false);
        }
        arrTasks.get(position).setIs_selected(true);
        adapter.notifyDataSetChanged();
    }
}
