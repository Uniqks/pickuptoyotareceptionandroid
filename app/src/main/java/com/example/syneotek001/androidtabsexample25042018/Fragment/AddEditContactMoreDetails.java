package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeaseSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;

public class AddEditContactMoreDetails extends Fragment {
    View view;
    ImageView ivback;
    Spinner spinnerGenderMainContact,spinnerGenderSpouse;
    ArrayList<String> countries;
    Button btnContinue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_edit_contact_more_details, container, false);
        ivback = view.findViewById(R.id.ivBack);
        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        spinnerGenderMainContact = view.findViewById(R.id.spinnerGenderMainContact);
        spinnerGenderSpouse = view.findViewById(R.id.spinnerGenderSpouse);
        btnContinue = view.findViewById(R.id.btnContinue);
        countries = new ArrayList<>();
        countries.add("Male");
        countries.add("Female");
        initCustomSpinner(countries);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < 6; ++i) {
                    fm.popBackStack();
                }
            }
        });
        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeaseSpinnerAdapter customSpinnerAdapter = new LeaseSpinnerAdapter(getActivity(), countries);
        spinnerGenderMainContact.setAdapter(customSpinnerAdapter);
        spinnerGenderMainContact.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerGenderSpouse.setAdapter(customSpinnerAdapter);
        spinnerGenderSpouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}

