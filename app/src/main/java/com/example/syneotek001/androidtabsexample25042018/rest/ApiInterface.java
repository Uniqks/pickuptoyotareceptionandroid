package com.example.syneotek001.androidtabsexample25042018.rest;


import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealInfoDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.LoginResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanActiveDealsResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanSoldDealsResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SendMessageResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("login.php")
    Call<LoginResponse> loginUser(@Query("email") String email,
                                  @Query("password") String password);

    @GET("chat.php")
    Call<ChatListResponse> getChatList(@Query("action") String action,
                                     @Query("user_id") String user_id,
                                     @Query("page") String page);

    @GET("chat.php")
    Call<ChatMessageResponse> getChatMessages(@Query("action") String action,
                                          @Query("user_id") String user_id,
                                          @Query("friend_id") String friend_id,
                                          @Query("page") String page);

    @GET("chat.php")
    Call<SendMessageResponse> sendChatMessages(@Query("action") String action,
                                              @Query("message") String message,
                                              @Query("user_id") String user_id,
                                              @Query("friend_id") String friend_id,
                                              @Query("msg_type") String msg_type);

    @FormUrlEncoded
    @POST("chat.php")
    Call<SendMessageResponse> uploadImageChat(@Field("action") String action,
                                               @Field("image") String image,
                                               @Field("user_id") String user_id,
                                               @Field("friend_id") String friend_id,
                                               @Field("msg_type") String msg_type);

    @GET("deal_manager.php")
    Call<SalesmanListResponse> getSalesmanList(@Query("action") String action,
                                           @Query("page") String page);

    @GET("deal_manager.php")
    Call<SalesmanDealRequestListResponse> getSalesmanDealRequestList(@Query("action") String action,
                                                          @Query("salesman_id") String salesman_id,
                                                          @Query("page") String page);

    @GET("deal_manager.php")
    Call<SalesmanActiveDealsResponse> getSalesmanActiveDeals(@Query("action") String action,
                                                                 @Query("salesman_id") String salesman_id,
                                                                 @Query("page") String page);

    @GET("deal_manager.php")
    Call<SalesmanSoldDealsResponse> getSalesmanSoldDeals(@Query("action") String action,
                                                         @Query("salesman_id") String salesman_id,
                                                         @Query("page") String page);

    @GET("deal_manager.php")
    Call<DealRequestDetailResponse> getDealRequestDetail(@Query("action") String action,
                                                         @Query("dr_id") String dr_id);

    @GET("deal_manager.php")
    Call<DealInfoDetailResponse> getDealInfoDetail(@Query("action") String action,
                                                      @Query("deal_id") String deal_id);

}
