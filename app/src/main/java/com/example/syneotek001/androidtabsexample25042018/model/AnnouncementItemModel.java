package com.example.syneotek001.androidtabsexample25042018.model;

public class AnnouncementItemModel {

    private String message = "";
    private String datetime = "";
    private String action = "";

    public AnnouncementItemModel(String message, String datetime, String action) {
        this.message = message;
        this.datetime = datetime;
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
