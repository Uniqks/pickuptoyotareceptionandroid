package com.example.syneotek001.androidtabsexample25042018.model;

public class CarModel {
    private String id = "";
    private String image = "";
    private boolean isSelected = false;
    private String name = "";
    private String price = "";

    public CarModel(String id, String name, String price, String image, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.isSelected = isSelected;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public String toString() {
        return "CarModel{id='" + this.id + '\'' + ", name='" + this.name + '\'' + ", price='" + this.price + '\'' + ", image='" + this.image + '\'' + ", isSelected=" + this.isSelected + '}';
    }
}
