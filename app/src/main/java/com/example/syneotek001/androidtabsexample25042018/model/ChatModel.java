package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;

public class ChatModel implements Serializable {
    private String id = "";
    private String image = "";
    private boolean isOnline = false;
    private long lastActive;
    private String name = "";
    private int unreadCount = 0;

    public ChatModel(String id, String name, String image, int unreadCount, boolean isOnline, long lastActive) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.unreadCount = unreadCount;
        this.isOnline = isOnline;
        this.lastActive = lastActive;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public boolean isOnline() {
        return this.isOnline;
    }

    public void setOnline(boolean online) {
        this.isOnline = online;
    }

    public long getLastActive() {
        return this.lastActive;
    }

    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }

    public String toString() {
        return "ChatModel{id='" + this.id + '\'' + ", name='" + this.name + '\'' + ", image='" + this.image + '\'' + ", unreadCount=" + this.unreadCount + ", isOnline=" + this.isOnline + ", lastActive=" + this.lastActive + '}';
    }
}
