package com.example.syneotek001.androidtabsexample25042018.model;

public class LeadItemModel {
    private long assignDate;
    private int businessLeadCount = 0;
    private String clientName = "";
    private String customerId = "";
    private String email = "";
    private String id = "";
    private boolean isSelected = false;
    private long lastActivity;
    private String leadLabel = "";
    private String leadStatus = "";
    private int pendingLeadCount = 0;
    private String phone = "";
    private String sourceLink = "";

    public LeadItemModel(String id, String customerId, String leadLabel, String clientName, String email, String phone, String sourceLink, String leadStatus, long assignDate, long lastActivity, int pendingLeadCount, int businessLeadCount) {
        this.id = id;
        this.customerId = customerId;
        this.leadLabel = leadLabel;
        this.clientName = clientName;
        this.email = email;
        this.phone = phone;
        this.sourceLink = sourceLink;
        this.leadStatus = leadStatus;
        this.assignDate = assignDate;
        this.lastActivity = lastActivity;
        this.pendingLeadCount = pendingLeadCount;
        this.businessLeadCount = businessLeadCount;
        this.isSelected = false;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLeadLabel() {
        return this.leadLabel;
    }

    public void setLeadLabel(String leadLabel) {
        this.leadLabel = leadLabel;
    }

    public String getClientName() {
        return this.clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSourceLink() {
        return this.sourceLink;
    }

    public void setSourceLink(String sourceLink) {
        this.sourceLink = sourceLink;
    }

    public String getLeadStatus() {
        return this.leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public long getAssignDate() {
        return this.assignDate;
    }

    public void setAssignDate(long assignDate) {
        this.assignDate = assignDate;
    }

    public long getLastActivity() {
        return this.lastActivity;
    }

    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }

    public int getPendingLeadCount() {
        return this.pendingLeadCount;
    }

    public void setPendingLeadCount(int pendingLeadCount) {
        this.pendingLeadCount = pendingLeadCount;
    }

    public int getBusinessLeadCount() {
        return this.businessLeadCount;
    }

    public void setBusinessLeadCount(int businessLeadCount) {
        this.businessLeadCount = businessLeadCount;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public String toString() {
        return "LeadItemModel{id='" + this.id + '\'' + ", customerId='" + this.customerId + '\'' + ", leadLabel='" + this.leadLabel + '\'' + ", clientName='" + this.clientName + '\'' + ", email='" + this.email + '\'' + ", phone='" + this.phone + '\'' + ", sourceLink='" + this.sourceLink + '\'' + ", leadStatus='" + this.leadStatus + '\'' + ", assignDate=" + this.assignDate + ", lastActivity=" + this.lastActivity + ", pendingLeadCount=" + this.pendingLeadCount + ", businessLeadCount=" + this.businessLeadCount + ", isSelected=" + this.isSelected + '}';
    }
}
