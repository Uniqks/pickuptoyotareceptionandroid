package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.syneotek001.androidtabsexample25042018.Adapter.LeaseSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentProfileStep1Binding;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentSalesmanProfileStep1Binding;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanItemModel;

import java.util.ArrayList;
import java.util.Arrays;

public class EditSalesmanFragment extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnSave, btnSaveAndNext;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;
    FragmentSalesmanProfileStep1Binding binding;
    String name, email, mobile, password, address;
    YoYo yoYo;
    ArrayList<String> arr_salesman_status = new ArrayList<>();

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salesman_profile_step1, container, false);
        View view = binding.getRoot();

 /*       btnSave = rootview.findViewById(R.id.btnSave);
        btnSaveAndNext = rootview.findViewById(R.id.btnSaveAndNext);
        etEmail = rootview.findViewById(R.id.etEmail);
        etFirstName = rootview.findViewById(R.id.etFirstName);
        etHomePhone = rootview.findViewById(R.id.etHomePhone);
        etLastName = rootview.findViewById(R.id.etLastName);
        etMobile = rootview.findViewById(R.id.etMobile);
        etTitle = rootview.findViewById(R.id.etTitle);
        etWorkFax = rootview.findViewById(R.id.etWorkFax);
        etWorkPhone = rootview.findViewById(R.id.etWorkPhone);
        ivContact = rootview.findViewById(R.id.ivContact);
        ivEmail = rootview.findViewById(R.id.ivEmail);
        ivMobile = rootview.findViewById(R.id.ivMobile);
        ivTitle = rootview.findViewById(R.id.ivTitle);
        tvPageIndicator = rootview.findViewById(R.id.tvPageIndicator);
        tvTitle = rootview.findViewById(R.id.tvTitle);
        ivBack = rootview.findViewById(R.id.ivBack);*/
        binding.ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        setHasOptionsMenu(false);
        setClickEvents();

        setAnimation(binding.lblName);
        setAnimation(binding.lblEmail);
        setAnimation(binding.lblMobileNumber);
        setAnimation(binding.lblPassword);
        setAnimation(binding.lblAddress);

        setAnimation(binding.etName);
        setAnimation(binding.etEmail);
        setAnimation(binding.etMobile);
        setAnimation(binding.etPassword);
        setAnimation(binding.etAddress);

        arr_salesman_status = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));
        initCustomSpinner(arr_salesman_status);


        if (getArguments() != null) {
            if (getArguments().getBoolean("is_edit")) {
//                profile_type
                if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                    if (getArguments().getString("profile_type").equals("salesman"))
                        binding.tvTitlemain.setText(getString(R.string.str_edit_sales));
                    else if (getArguments().getString("profile_type").equals("receptionist"))
                        binding.tvTitlemain.setText(getString(R.string.str_edit_receptionist));
                }
                if (getArguments().getSerializable("salesman_profile") != null) {
                    SalesmanItemModel salesmanItemModel = (SalesmanItemModel) getArguments().getSerializable("salesman_profile");
                    if (salesmanItemModel.getStatus().equals("1")) {
                        binding.spinnerStatus.setSelection(0);
                    } else if (salesmanItemModel.getStatus().equals("2")) {
                        binding.spinnerStatus.setSelection(1);
                    } else if (salesmanItemModel.getStatus().equals("3")) {
                        binding.spinnerStatus.setSelection(2);
                    }

                    binding.etName.setText(salesmanItemModel.getFirstName());
                    binding.etLastName.setText(salesmanItemModel.getLastName());
                    binding.etEmail.setText(salesmanItemModel.getEmail());
                    binding.etMobile.setText(salesmanItemModel.getPhone());
                    binding.etPassword.setText(salesmanItemModel.getPassword());
                    binding.etAddress.setText(salesmanItemModel.getAddress());
                }
            } else {
                if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                    if (getArguments().getString("profile_type").equals("salesman"))
                        binding.tvTitlemain.setText(getString(R.string.str_add_sales));
                    else if (getArguments().getString("profile_type").equals("receptionist"))
                        binding.tvTitlemain.setText(getString(R.string.str_add_receptionist));
                }
            }
        }


        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeaseSpinnerAdapter customSpinnerAdapter = new LeaseSpinnerAdapter(getActivity(), countries);
        binding.spinnerStatus.setAdapter(customSpinnerAdapter);
        binding.spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setAnimation(View view) {
        YoYo.with(Techniques.FlipInX).duration(1000).delay(150).repeat(0).playOn(view);
    }

    private void setClickEvents() {
        binding.btnSaveAndNext.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSaveAndNext:
                EditSalesmanFragmentstep2 editSalesmanFragmentstep2 = new EditSalesmanFragmentstep2();
                if (getArguments() != null) {
                    if (getArguments().getBoolean("is_edit")) {
                        if (getArguments().getSerializable("salesman_profile") != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("salesman_profile", getArguments().getSerializable("salesman_profile"));
                            bundle.putBoolean("is_edit", true);
                            editSalesmanFragmentstep2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(editSalesmanFragmentstep2);
                        }
                    } else {
                        if (getArguments().getString("profile_type")!=null && !getArguments().getString("profile_type").equals("")){
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("is_edit", false);
                            bundle.putString("profile_type",getArguments().getString("profile_type"));
                            editSalesmanFragmentstep2.setArguments(bundle);
                            ((HomeActivity) getActivity()).replaceFragment(editSalesmanFragmentstep2);
                        }
                    }
                }
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
