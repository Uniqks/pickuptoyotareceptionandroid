package com.example.syneotek001.androidtabsexample25042018.interfaces;
public interface OnLeadItemViewClickListener extends OnRecyclerViewItemClickListener {
    void onLeadBusinessRequestClicked(int i);

    void onLeadDealRequestClicked(int i);

    void onLeadDetailIconClicked(int i);

    void onLeadEmailIconClicked(int i);

    void onLeadInactiveIconClicked(int i);

    void onLeadLabelClicked(int i);

    void onLeadScheduleIconClicked(int i);
}
