package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RecyclerViewFastScroller extends LinearLayout {
    private static final int BUBBLE_ANIMATION_DURATION = 100;
    private static final int TRACK_SNAP_RANGE = 5;
    private TextView bubble;
    private ObjectAnimator currentAnimator = null;
    private View handle;
    private int height;
    private boolean isInitialized = false;
    private final OnScrollListener onScrollListener = new C06131();
    private RecyclerView recyclerView;

    public interface BubbleTextGetter {
        String getTextToShowInBubble(int i);
    }

    class C06131 extends OnScrollListener {
        C06131() {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            RecyclerViewFastScroller.this.updateBubbleAndHandlePosition();
        }
    }

    class C06142 extends AnimatorListenerAdapter {
        C06142() {
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            RecyclerViewFastScroller.this.bubble.setVisibility(INVISIBLE);
            RecyclerViewFastScroller.this.currentAnimator = null;
        }

        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            RecyclerViewFastScroller.this.bubble.setVisibility(INVISIBLE);
            RecyclerViewFastScroller.this.currentAnimator = null;
        }
    }

    public RecyclerViewFastScroller(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public RecyclerViewFastScroller(Context context) {
        super(context);
        init(context);
    }

    public RecyclerViewFastScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void init(Context context) {
        if (!this.isInitialized) {
            this.isInitialized = true;
            setOrientation(VERTICAL);
            setClipChildren(false);
        }
    }

    public void setViewsToUse(@LayoutRes int layoutResId, @IdRes int bubbleResId, @IdRes int handleResId) {
        LayoutInflater.from(getContext()).inflate(layoutResId, this, true);
        this.bubble = (TextView) findViewById(bubbleResId);
        if (this.bubble != null) {
            this.bubble.setVisibility(INVISIBLE);
        }
        this.handle = findViewById(handleResId);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.height = h;
        updateBubbleAndHandlePosition();
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (event.getX() >= this.handle.getX() - ((float) ViewCompat.getPaddingStart(this.handle))) {
                    if (this.currentAnimator != null) {
                        this.currentAnimator.cancel();
                    }
                    if (this.bubble != null && this.bubble.getVisibility() == INVISIBLE) {
                        showBubble();
                    }
                    this.handle.setSelected(true);
                    break;
                }
                return false;
            case 1:
            case 3:
                this.handle.setSelected(false);
                hideBubble();
                return true;
            case 2:
                break;
            default:
                return super.onTouchEvent(event);
        }
        float y = event.getY();
        setBubbleAndHandlePosition(y);
        setRecyclerViewPosition(y);
        return true;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        if (this.recyclerView != recyclerView) {
            if (this.recyclerView != null) {
                this.recyclerView.removeOnScrollListener(this.onScrollListener);
            }
            this.recyclerView = recyclerView;
            if (this.recyclerView != null) {
                recyclerView.addOnScrollListener(this.onScrollListener);
            }
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.recyclerView != null) {
            this.recyclerView.removeOnScrollListener(this.onScrollListener);
            this.recyclerView = null;
        }
    }

    private void setRecyclerViewPosition(float y) {
        if (this.recyclerView != null) {
            float proportion;
            int itemCount = this.recyclerView.getAdapter().getItemCount();
            if (this.handle.getY() == 0.0f) {
                proportion = 0.0f;
            } else if (this.handle.getY() + ((float) this.handle.getHeight()) >= ((float) (this.height - 5))) {
                proportion = 1.0f;
            } else {
                proportion = y / ((float) this.height);
            }
            int targetPos = getValueInRange(0, itemCount - 1, (int) (((float) itemCount) * proportion));
            ((LinearLayoutManager) this.recyclerView.getLayoutManager()).scrollToPositionWithOffset(targetPos, 0);
            String bubbleText = ((BubbleTextGetter) this.recyclerView.getAdapter()).getTextToShowInBubble(targetPos);
            if (this.bubble != null) {
                this.bubble.setText(bubbleText);
            }
        }
    }

    private int getValueInRange(int min, int max, int value) {
        return Math.min(Math.max(min, value), max);
    }

    public void updateBubbleAndHandlePosition() {
        if (this.bubble != null && !this.handle.isSelected()) {
            setBubbleAndHandlePosition(((float) this.height) * (((float) this.recyclerView.computeVerticalScrollOffset()) / (((float) this.recyclerView.computeVerticalScrollRange()) - ((float) this.height))));
        }
    }

    private void setBubbleAndHandlePosition(float y) {
        int handleHeight = this.handle.getHeight();
        this.handle.setY((float) getValueInRange(0, this.height - handleHeight, (int) (y - ((float) (handleHeight / 2)))));
        if (this.bubble != null) {
            int bubbleHeight = this.bubble.getHeight();
            this.bubble.setY((float) getValueInRange(0, (this.height - bubbleHeight) - (handleHeight / 2), (int) (y - ((float) bubbleHeight))));
        }
    }

    private void showBubble() {
        if (this.bubble != null) {
            this.bubble.setVisibility(VISIBLE);
            if (this.currentAnimator != null) {
                this.currentAnimator.cancel();
            }
            this.currentAnimator = ObjectAnimator.ofFloat(this.bubble, "alpha", new float[]{0.0f, 1.0f}).setDuration(100);
            this.currentAnimator.start();
        }
    }

    private void hideBubble() {
        if (this.bubble != null) {
            if (this.currentAnimator != null) {
                this.currentAnimator.cancel();
            }
            this.currentAnimator = ObjectAnimator.ofFloat(this.bubble, "alpha", new float[]{1.0f, 0.0f}).setDuration(100);
            this.currentAnimator.addListener(new C06142());
            this.currentAnimator.start();
        }
    }
}
