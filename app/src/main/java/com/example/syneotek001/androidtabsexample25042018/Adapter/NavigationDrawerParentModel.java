package com.example.syneotek001.androidtabsexample25042018.Adapter;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class NavigationDrawerParentModel extends ExpandableGroup<NavigationDrawerChildModel> {
    private int iconResId;

    public NavigationDrawerParentModel(String title, List<NavigationDrawerChildModel> items, int iconResId) {
        super(title, items);
        this.iconResId = iconResId;
    }

    public int getIconResId() {
        return this.iconResId;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NavigationDrawerParentModel)) {
            return false;
        }
        if (getIconResId() != ((NavigationDrawerParentModel) o).getIconResId()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return getIconResId();
    }
}
